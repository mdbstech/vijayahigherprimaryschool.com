<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->BigIncrements('user_id');
            $table->string('name',50);
            $table->string('email',50)->unique();
            $table->string('password',255);
            $table->string('mobile_no',50)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->text('address')->nullable();
            $table->string('avatar',50)->default('avatar.png');
            $table->rememberToken();
            $table->timestamps();
        });

         DB::table('users')->insert(
        [
            [
                'name' => 'MDBS Tech Private Limited',
                'email' => 'mdbstech@gmail.com',
                'password' => Hash::make('1qaz2wsx'),
                'mobile_no' => '9535342875',
                'address' => 'Mysore',
                'avatar' => 'avatar.png',
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
