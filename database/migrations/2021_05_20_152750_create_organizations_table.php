<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('organizations', function (Blueprint $table) {
            $table->BigIncrements('org_id');
            $table->string('org_code',50);
            $table->string('org_name',50);
            $table->string('email',50)->nullable();
            $table->string('mobile_no',50)->nullable();
            $table->string('phone_no',50)->nullable();
            $table->string('website',50)->nullable();
            $table->text('address')->nullable();
            $table->string('logo',50)->default('logo.png');
            $table->timestamps();
        });
        DB::table('organizations')->insert(
            [
                [
                    'org_code' => 'MDBSTECH',
                    'org_name' => 'MDBS Tech Private Limited',
                    'email' => 'mdbstech@gmail.com',
                    'mobile_no' => '9535342875',
                    'phone_no' => '08214265003',
                    'website' => 'https://www.mdbstech.com',
                    'address' => 'Mysore',
                ],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
