<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
   protected $fillable = [
        'title','slug','description','poster'
    ];
    protected $primaryKey = 'album_id';

    public function Photos()
    {
    	return $this->hasMany('App\Photo','album_id','album_id');
    }
}
