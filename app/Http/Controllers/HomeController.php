<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Album;
use App\Photo;
use Image;

class HomeController extends Controller
{
         /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
     public function dashboard()
    {
        $albumcount = Album::get()->count();
        $photocount = Photo::get()->count();
        return view('admin.dashboard',compact('albumcount','photocount'));
    }


     public function home()
    {
        return view('home');
    }

     public function about()
    {
        return view('about');
    }

     public function contact()
    {
        return view('contact');
    }

    public function gallery()
    {
        $albums = Album::has('Photos')->get();
        $photos = Photo::get();
        return view ('gallery',compact('albums','photos'));
    }

      public function gallery1()
    {
        return view('gallery1');
    }

     public function academics()
    {
        return view('academics');
    }

     public function activities()
    {
        return view('activities');
    }


    public function facilities()
    {
        return view('facilities');
    }

      public function beyondacademics()
    {
        return view('beyondacademics');
    }

       public function staffs()
    {
        return view('staffs');
    }

      public function management()
    {
        return view('management');
    }

        public function students()
    {
        return view('students');
    }

         public function norms()
    {
        return view('norms');
    }

           public function circular()
    {
        return view('circular');
    }


           public function annualreport()
    {
        return view('annualreport');
    }

            public function gallery2()
    {
        return view('gallery2');
    }


}
