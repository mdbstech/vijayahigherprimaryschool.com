<?php

namespace App\Http\Controllers;

use App\Organization;
use Illuminate\Http\Request;
use App\User;
use Image;
use Auth;
use Hash;

class OrganizationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

   public function index()
    {
       
        $org = Organization::first();
        return view('admin.organization',compact('org'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        
    }

    public function show(Organization $organization)
    {
        
    }

    public function edit(Organization $organization)
    {
        
    }

    public function update(Request $request, Organization $organization)
    {
           $data = $this->validate($request, [
            'org_code' => 'required|max:50',
            'org_name' => 'required|max:50',
            'email' => 'sometimes|nullable|email|max:50',
            'mobile_no' => 'sometimes|nullable|regex:/[0-9]{10}/',
            'phone_no' => 'sometimes|nullable|regex:/[0-9]{10}/',
            'website' => 'sometimes|nullable|url|max:50',
            'address' => 'nullable|max:2550',
        ]);
        if($request->hasFile('logo'))
        {
            $file = $request->file('logo');
            $filename = date('Ymdhis'). '.' . $file->getClientOriginalExtension();
            Image::make($file)->save(public_path('/storage/organization/'.$filename));
            $data['logo'] = $filename;
        }
        Organization::where('org_id',$organization->org_id)->update($data);
        return redirect()->back()->with('success','Organization is successfuly updated');
    }

    public function destroy(Organization $organization)
    {
        
    }

     public function profile()
    {
        return view('admin.user.profile');
    }

    public function update_profile(Request $request)
    {
        $user = Auth::User();
        $data = $this->validate($request, [
            'name'=>'required|max:50',
            'email'=>'required|email|max:50|regex:/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$/|unique:users,email,'.$user->user_id.',user_id',
            'mobile_no' => 'required|regex:/[0-9]{10}/|digits:10|unique:users,mobile_no,'.$user->user_id.',user_id',
            'address'=>'nullable',
            'avatar' => 'sometimes|nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
           
        ]);
        if($request->hasFile('avatar'))
        {
            $file = $request->file('avatar');
            $filename = date('Ymdhis'). '.' . $file->getClientOriginalExtension();
            Image::make($file)->save(public_path('/storage/user/'.$filename));
            $data['avatar'] = $filename;
        }
        User::where('user_id',$user->user_id)->update($data);
        return redirect()->route('profile')->with('success','User Profile is successfully updated');  
    }

    public function changepassword()
    {
        return view('admin.user.changepassword');
    }

     public function password(Request $request)
    {
        $data = $this->validate($request, [
            'current_password' => 'required|min:8',
            'new_password' => 'required|min:8',
            'confirm_password' => 'required|min:8',
        ]);
        if (!(Hash::check($request->current_password, Auth::user()->password))) 
        {
            return redirect()->route('changepassword')->with("error","Your current password does not matches with the password you provided. Please try again.");
        }
        else if(strcmp($request->current_password, $request->new_password) == 0)
        {
            return redirect()->route('changepassword')->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }
        else if(strcmp($request->new_password, $request->confirm_password) != 0)
        {
            return redirect()->route('changepassword')->with("error","The password confirmation does not match.");
        }
        else
        {
            User::where('user_id',Auth::User()->user_id)->update([
                'password' => Hash::make($request->new_password),
            ]);
            return redirect()->route('changepassword')->with('success','Password Updated Successfully');
        }
    }
}
