<?php

namespace App\Http\Controllers;

use App\Photo;
use Illuminate\Http\Request;

class PhotoController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $album = Album::where('album_id',$request->album_id)->first();
        $images = Photo::where('album_id',$request->album_id)->get();
        return view('album.show',compact('images','album'));
    }


    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        $this->validate($request, [
            'album_id'=>'required|max:50',
            'images'=> 'required',
        ]);
        foreach ($request->images as $image) {
            $filename = $image->store('photos');
            $filedata = ltrim($filename,'photos/');
            Photo::create([
                'album_id' => $request->album_id,
                'image' => $filedata
            ]);
        }
        return redirect()->back()->with('success','Image is successfully created...!');
    }

   
    public function show(Photo $photo)
    {
        //
    }

   
    public function edit(Photo $photo)
    {
        //
    }

    
    public function update(Request $request, Photo $photo)
    {
        //
    }

       public function destroy(Photo $photo)
    {
         Photo::where('photo_id',$photo->photo_id)->Delete();
        session()->flash('success', 'Image is successfully deleted...!!');
    }
}
