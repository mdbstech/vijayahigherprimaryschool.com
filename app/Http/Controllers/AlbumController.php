<?php

namespace App\Http\Controllers;
use Image;
use App\Album;
use Illuminate\Http\Request;

use App\Photo;

class AlbumController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth');
    } 

    public function index()
    {
        $albums = Album::get();
        return view('admin.album.index',compact('albums'));
    }

    public function create()
    {
        return view('admin.album.create');
    }

    public function store(Request $request)
    {
        $data = $this->validate($request, [
                'title' => 'required|max:150',
                'slug' => 'required|max:100|unique:albums',               
                'description' => 'nullable',
                'poster' => 'required',
            ]);
           if($request->hasFile('poster'))
            {
                $file = $request->file('poster');
                $filename = date('Ymdhis'). '.' . $file->getClientOriginalExtension();
                Image::make($file)->save(public_path('/storage/album/'.$filename));
                $data['poster'] = $filename;
            }

            $album = Album::create($data);
            return redirect()->route('albums.index')->with('success','Album is successfully created...!');
    }
    
    public function show(Album $album)
    {
         $images = Photo::where('album_id',$album->album_id)->get();
        return view('admin.album.show',compact('images','album'));
    }

    public function edit(Album $album)
    {
        return view('admin.album.edit',compact('album'));
    }
   
    public function update(Request $request, Album $album)
    {
        $data = $this->validate($request, [
            'title' => 'required|max:150',
            'slug' => 'required|max:150|unique:albums,slug,'.$album->album_id.',album_id',
            'description' => 'nullable',
            'poster' => 'nullable',
        ]);
       if($request->hasFile('poster'))
        {
            $file = $request->file('poster');
            $filename = date('Ymdhis'). '.' . $file->getClientOriginalExtension();
            Image::make($file)->save(public_path('/storage/album/'.$filename));
            $data['poster'] = $filename;
        }
        Album::where('album_id',$album->album_id)->update($data);
        return redirect()->route('albums.index')->with('success','Album is successfully updated...!');
    }
    
    public function destroy(Album $album)
    {
        Album::where('album_id',$album->album_id)->with('Image')->delete();
        session()->flash('success', 'Album is successfully deleted...!!');
    }
}
