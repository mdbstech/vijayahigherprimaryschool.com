<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = [
        'album_id','image'
    ];
    protected $primaryKey = 'photo_id';
    
    public function Album()
    {
    	return $this->hasOne('App\Album', 'album_id', 'album_id');
    }
}
