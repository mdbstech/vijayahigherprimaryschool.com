@extends('layouts.appfront')

@section('content')

 <!--Page Title-->
    <section class="page-title" style="background-image:url(img/banner.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
              
                <h1>Staffs</h1>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Program Section -->
    <section class="program-section alternate">
        <div class="auto-container">
            <div class="row">
                <!-- Program Block -->
                <div class="program-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/staff.jpg') }}" style="height:400px;" alt=""></figure>
                        </div>
                 
                    </div>
                </div>

                       <!-- Program Block -->
                <div class="program-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                   
                        <div class="lower-content">
                            <h4>Teachers Desk</h4>
                            <div class="text" align="justify">Vijaya Kids Care is part of Vidya Prachara Sangha a Seventy Year old institution set up by visionary founders and managed by a forward looking management.<br>
							The school management is committed to nurture the bright children from rural and humble settings in and around Pandavapura to blossom into confident citizens of tomorrow with good value systems.<br>
							You will please appreciate most of the teachers and other staff members are also from a similar background .However with my many years of diverse career in schools, I am trying to groom them to adapt to evolving trends in primary education and prepare the children to be on par with the best schools.
							</div>
                           
                        </div>
                    </div>
                </div>



			</div>
		</div>
	</section>


	   <!-- Team Section -->
    <section class="team-section">
        <div class="auto-container">
            <div class="sec-title text-center">
               
                <h2>Teachers</h2>
            </div>

            <div class="row">
                <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/teachers/1.jpg') }}" style="height: 180px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name">Sowmyashree P</h6>
                            <p>Mcom, B.Ed</p>
                             <span class="designation">Principal</span>
                         
                        </div>
                    </div>
                </div>

                    <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/teachers/4.jpg') }}" style="height: 180px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name">Leela H L</h6>
                            <p>MA, B.Ed</p>

                              <span class="designation">AT</span>
                         
                        </div>
                    </div>
                </div>
  
                <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/teachers/2.jpg') }}" style="height: 180px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name">Roopa</h6>
                             <p>BA, B.Ed</p>
                             <span class="designation">AT</span>
                            
                         
                        </div>
                    </div>
                </div>
       
                <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/teachers/3.jpg') }}" style="height: 180px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name">Arpitha L</h6>
                            <p>MSC, B.Ed</p>
                            <span class="designation">AT</span>

                         
                        </div>
                    </div>
                </div>


              


                  <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/teachers/5.jpg') }}" style="height: 180px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name">Pavithra H M</h6>
                            <p>MA, B.Ed</p>
                            <span class="designation">AT</span>
                         
                        </div>
                    </div>
                </div>

                  <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/teachers/6.jpg') }}" style="height: 180px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name">Parimala N</h6>
                            <p>PUC, D.Ed</p>
                            <span class="designation">AT</span>
                         
                        </div>
                    </div>
                </div>

                  <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/teachers/7.jpg') }}" style="height: 180px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name">Shruthi</h6>
                             <p>BA, B.Ed</p>
                             <span class="designation">AT</span>
                         
                        </div>
                    </div>
                </div>

                  <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/teachers/8.jpg') }}" style="height: 180px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name">Tejamani</h6>
                            <p>MA, B.Ed</p>
                            <span class="designation">AT</span>
                         
                        </div>
                    </div>
                </div>

                  <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/teachers/9.jpg') }}" style="height: 180px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name">Chandrakala M P</h6>
                             <p>BA, B.Ed</p>
                             <span class="designation">AT</span>
                         
                        </div>
                    </div>
                </div>

                  <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/teachers/10.jpg') }}" style="height: 180px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name">Navya S</h6>
                            <p>MA, B.Ed</p>
                            <span class="designation">AT</span>
                         
                        </div>
                    </div>
                </div>

                  <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/teachers/11.jpg') }}" style="height: 180px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name">Savitha B C</h6>
                            <p>MA, B.Ed</p>
                            <span class="designation">AT</span>
                         
                        </div>
                    </div>
                </div>

                  <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/teachers/12.jpg') }}" style="height: 180px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name">Shobha H K</h6>
                            <p>BA, B.Ed</p>
                            <span class="designation">AT</span>
                         
                        </div>
                    </div>
                </div>

                       <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/teachers/13.jpg') }}" style="height: 180px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name">Deepa R</h6>
                            <p>PUC(PCMB), Bcom</p>
                            <span class="designation">AT</span>
                         
                        </div>
                    </div>
                </div>

                       <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/teachers/14.jpg') }}" style="height: 180px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name">Namithanjali D N</h6>
                             <p>MSC, B.Ed</p>
                             <span class="designation">AT</span>
                         
                        </div>
                    </div>
                </div>

                       <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/teachers/15.jpg') }}" style="height: 180px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name">Manjula S M</h6>
                             <p>BA, B.Ed</p>
                             <span class="designation">AT</span>
                         
                        </div>
                    </div>
                </div>

                       <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/teachers/16.jpg') }}" style="height: 180px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name">Susheela</h6>
                            <p>MA, B.Ed</p>
                            <span class="designation">AT</span>
                         
                        </div>
                    </div>
                </div>

                       <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/teachers/17.jpg') }}" style="height: 180px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name">Pavithra S</h6>
                             <p>BA, B.Ed</p>
                             <span class="designation">AT</span>
                         
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>


    @endsection