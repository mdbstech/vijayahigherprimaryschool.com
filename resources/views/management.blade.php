@extends('layouts.appfront')

@section('css')

<style type="text/css">
    table { 
    width: 750px; 
    border-collapse: collapse; 
    margin:50px auto;
    }

/* Zebra striping */
tr:nth-of-type(odd) { 
    background: #eee; 
    }

th { 
    background: #3498db; 
    color: white; 
    font-weight: bold; 
    }

td, th { 
    padding: 10px; 
    border: 1px solid #ccc; 
    text-align: left; 
    font-size: 18px;
    }

/* 
Max width before this PARTICULAR table gets nasty
This query will take effect for any screen smaller than 760px
and also iPads specifically.
*/
@media 
only screen and (max-width: 760px),
(min-device-width: 768px) and (max-device-width: 1024px)  {

    table { 
        width: 100%; 
    }

    /* Force table to not be like tables anymore */
    table, thead, tbody, th, td, tr { 
        display: block; 
    }
    
    /* Hide table headers (but not display: none;, for accessibility) */
    thead tr { 
        position: absolute;
        top: -9999px;
        left: -9999px;
    }
    
    tr { border: 1px solid #ccc; }
    
    td { 
        /* Behave  like a "row" */
        border: none;
        border-bottom: 1px solid #eee; 
        position: relative;
        padding-left: 50%; 
    }

    td:before { 
        /* Now like a table header */
        position: absolute;
        /* Top/left values mimic padding */
        top: 6px;
        left: 6px;
        width: 45%; 
        padding-right: 10px; 
        white-space: nowrap;
        /* Label the data */
        content: attr(data-column);

        color: black;
        font-weight: bold;
    }

}
</style>
@endsection

@section('content')

  <!--Page Title-->
    <section class="page-title" style="background-image:url(img/banner.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
              
                <h1>Mangement Committee</h1>
            </div>
        </div>
    </section>
    <!--End Page Title-->


 <section class="testimonial-section">
        <div class="auto-container">
            <div class="row">
                    <table>
                      <thead>
                        <tr>
                            <th>SL No.</th>
                          <th>Member Name</th>
                          <th>Father/Spouse Name</th>
                          <th>Designation in SMC</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td data-column="First Name"> 1 </td>
                          <td data-column="Last Name">C S PUTTARAJU</td>
                          <td data-column="Job Title">SANNA THAMMEGOWDA</td>
                          <td data-column="Twitter">PRESIDENT</td>
                                    
                        </tr>
                        <tr>
                          <td data-column="First Name">2</td>
                          <td data-column="Last Name">SOWMYA SHREE P</td>
                          <td data-column="Job Title">JAGADEESH A K</td>
                          <td data-column="Twitter">SECRETARY</td>
                        </tr>
                        <tr>
                          <td data-column="First Name">3</td>
                          <td data-column="Last Name">DEEPIKA</td>
                          <td data-column="Job Title">UMESH</td>
                          <td data-column="Twitter">DIRECTOR</td>
                        </tr>
                        <tr>
                          <td data-column="First Name">4</td>
                          <td data-column="Last Name">SOMASHEKAR K</td>
                          <td data-column="Job Title">KENCHEGOWDA</td>
                          <td data-column="Twitter">DIRECTOR</td>
                        </tr>
                        <tr>
                          <td data-column="First Name">5</td>
                          <td data-column="Last Name">SAVITHA B C</td>
                          <td data-column="Job Title">VARADARAJU S K</td>
                          <td data-column="Twitter">DIRECTOR</td>
                        </tr>


                        <tr>
                            <td data-column="First Name">6</td>
                            <td data-column="Last Name">PAVITHRA H M </td>
                            <td data-column="Job Title">PRAVEEN M </td> 
                            <td data-column="Twitter">DIRECTOR</td>
                        </tr>

                         <tr>
                            <td data-column="First Name">7</td>
                              <td data-column="Last Name">SOWJANYA B  </td>
                              <td data-column="Job Title">MANJUNATHA A </td>   
                              <td data-column="Twitter">DIRECTOR </td>
                        </tr>

                         <tr>
                            <td data-column="First Name">8</td>
                                <td data-column="Last Name">MADHU H B  </td> 
                               <td data-column="Job Title"> BASAVARAJU </td> 
                               <td data-column="Twitter"> DIRECTOR </td>
                        </tr>

                         <tr>
                            <td data-column="First Name">9</td>
                                <td data-column="Last Name">BASAVARAJU K V </td> 
                               <td data-column="Job Title"> EREGOWDA</td>    
                               <td data-column="Twitter"> DIRECTOR</td>
                        </tr>

                         <tr>
                            <td data-column="First Name">10</td>
                               <td data-column="Last Name"> MARISWAMYGOWDA </td> 
                               <td data-column="Job Title"> SIDDEGOWDA  </td>
                                <td data-column="Twitter">DIRECTOR</td>
                        </tr>

                         <tr>
                            <td data-column="First Name">11</td>
                               <td data-column="Last Name"> RAMEGOWDA N  </td>   
                               <td data-column="Job Title"> NADEGOWDA  </td> 
                               <td data-column="Twitter"> DIRECTOR</td>
                        </tr>

                        <tr>
                            <td data-column="First Name">12</td>
                                <td data-column="Last Name">GOPALASWAMY B S </td>    
                                <td data-column="Job Title">SHASHIGOWDA   </td>  
                                <td data-column="Twitter">DIRECTOR</td>
                        </tr>

                        <tr>
                            <td data-column="First Name">13</td>
                                <td data-column="Last Name">MAYIGOWDA </td>  
                               <td data-column="Job Title"> MAYIGOWDA </td>  
                                <td data-column="Twitter">DIRECTOR </td>
                        </tr>

                        <tr>
                            <td data-column="First Name">14</td>
                                <td data-column="Last Name">SHANTHAMMA  </td>
                                <td data-column="Job Title">LATE MAHADEVU  </td>
                                <td data-column="Twitter"> DIRECTOR </td>
                        </tr>

                        <tr>
                            <td data-column="First Name">15</td>
                                <td data-column="Last Name">KUMARA SWAMY K B  </td>  
                                <td data-column="Job Title">BASAVARAJU K V </td>
                                <td data-column="Twitter">DIRECTOR</td>
                        </tr>



                      </tbody>
                    </table>
                </div>
            </div>
        </section>
  

@endsection