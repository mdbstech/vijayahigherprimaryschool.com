@extends('layouts.appfront')

@section('css')

<style type="text/css">
    table { 
    width: 750px; 
    border-collapse: collapse; 
    margin:50px auto;
    }

/* Zebra striping */
tr:nth-of-type(odd) { 
    background: #eee; 
    }

th { 
    background: #3498db; 
    color: white; 
    font-weight: bold; 
    }

td, th { 
    padding: 10px; 
    border: 1px solid #ccc; 
    text-align: left; 
    font-size: 18px;
    }

/* 
Max width before this PARTICULAR table gets nasty
This query will take effect for any screen smaller than 760px
and also iPads specifically.
*/
@media 
only screen and (max-width: 760px),
(min-device-width: 768px) and (max-device-width: 1024px)  {

    table { 
        width: 100%; 
    }

    /* Force table to not be like tables anymore */
    table, thead, tbody, th, td, tr { 
        display: block; 
    }
    
    /* Hide table headers (but not display: none;, for accessibility) */
    thead tr { 
        position: absolute;
        top: -9999px;
        left: -9999px;
    }
    
    tr { border: 1px solid #ccc; }
    
    td { 
        /* Behave  like a "row" */
        border: none;
        border-bottom: 1px solid #eee; 
        position: relative;
        padding-left: 50%; 
    }

    td:before { 
        /* Now like a table header */
        position: absolute;
        /* Top/left values mimic padding */
        top: 6px;
        left: 6px;
        width: 45%; 
        padding-right: 10px; 
        white-space: nowrap;
        /* Label the data */
        content: attr(data-column);

        color: black;
        font-weight: bold;
    }

}
</style>
@endsection

@section('content')

  <!--Page Title-->
    <section class="page-title" style="background-image:url(img/banner.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
              
                <h1>Number Of Students Class Wise 2019-20</h1>
            </div>
        </div>
    </section>
    <!--End Page Title-->


 <section class="testimonial-section">
        <div class="auto-container">
            <div class="row">
                    <table>
                      <thead>
                        <tr>
                            <th>SL No.</th>
                          <th>Class</th>
                          <th>Girls</th>
                          <th>Boys</th>
                          <th>Total</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td> 1 </td>
                          <td>Pre KG</td>
                          <td>7</td>
                          <td>7</td>
                          <td>14</td>
                                    
                        </tr>
                        <tr>
                          <td>2</td>
                         <td>LKG</td>
                          <td>17</td>
                          <td>25</td>
                          <td>42</td>

                        </tr>
                        <tr>
                          <td>3</td>
                          <td>UKG</td>
                          <td>25</td>
                          <td>34</td>
                          <td>59</td>

                        </tr>
                        <tr>
                          <td>4</td>
                          <td>I STD</td>
                          <td>29</td>
                          <td>21</td>
                          <td>50</td>

                        </tr>
                        <tr>
                          <td>5</td>
                          <td>II STD</td>
                          <td>21</td>
                          <td>25</td>
                          <td>46</td>
                        </tr>

                        <tr>
                          <td>6</td>
                          <td>III STD</td>
                          <td>19</td>
                          <td>19</td>
                          <td>38</td>
                        </tr>

                        <tr>
                          <td>7</td>
                          <td>IV STD</td>
                          <td>20</td>
                          <td>19</td>
                          <td>39</td>
                        </tr>

                        <tr>
                          <td>8</td>
                          <td>V STD</td>
                          <td>15</td>
                          <td>12</td>
                          <td>27</td>
                        </tr>

                        <tr>
                          <td>9</td>
                          <td>VI STD</td>
                          <td>4</td>
                          <td>5</td>
                          <td>9</td>
                        </tr>

                          <tr>
                          <td>10</td>
                          <td>VII STD</td>
                          <td>4</td>
                          <td>3</td>
                          <td>7</td>
                        </tr>




                      </tbody>
                    </table>
                </div>
            </div>
        </section>
  

@endsection