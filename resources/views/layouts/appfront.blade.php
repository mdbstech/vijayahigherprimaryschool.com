<!DOCTYPE html>
<html lang="en">


<head>
<meta charset="utf-8">
<title>Vijaya Higher Primary School</title>
<!-- Stylesheets -->
<link href="{{ asset ('css/bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset ('plugins/revolution/css/settings.css') }}" rel="stylesheet" type="text/css"><!-- REVOLUTION SETTINGS STYLES -->
<link href="{{ asset ('plugins/revolution/css/layers.css') }}" rel="stylesheet" type="text/css"><!-- REVOLUTION LAYERS STYLES -->
<link href="{{ asset ('plugins/revolution/css/navigation.css') }}" rel="stylesheet" type="text/css"><!-- REVOLUTION NAVIGATION STYLES -->

<link href="{{ asset ('css/style.css') }}" rel="stylesheet">
<link href="{{ asset ('css/responsive.css') }}" rel="stylesheet">

<link rel="shortcut icon" href="{{ asset ('img/logo.png') }}" type="image/x-icon">
<link rel="icon" href="{{ asset ('img/logo.png') }}" type="image/x-icon">

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js') }}"></script><![endif]-->
<!--[if lt IE 9]><script src="{{ asset ('js/respond.js') }}"></script><![endif]-->

     <style type="text/css">
         html {
                  scroll-behavior: smooth;
               }
         </style>

         @yield('css')
</head>

<body>

<div class="page-wrapper">
    <!-- Preloader -->
   <!--  <div class="preloader"></div> -->

    <header class="main-header header-style-two">
        <!-- Header Top -->
        <div class="header-top d-none d-sm-block">
            <div class="auto-container">
                <div class="clearfix">
                    <div class="top-left">
                        <ul class="info-list clearfix">
                            <li><a href="tel:6668880000"><span class="fa fa-phone-square"></span>+91 7090447609</a></li>
                            <li><a href="mailto:info@bebio.com"><span class="fa fa-envelope"></span>vhlpschool@gmail.com</a></li>

                              <li><a href="{{ url ('contact-us')}}"><span class="fa fa-phone"></span>Contact Us</a></li>

                              <li><span class="fa fa-map"></span>State Board Recognition No. : 21/2013-14</li>

                              <li><span class="fa fa-map"></span>Dice Code : 29220618702</li>
                        </ul>
                    </div>

                    <div class="top-right">
                      
                      <!-- Main Menu End-->
                    <div class="outer-box clearfix" style="padding-top: 10px;">
                        <ul class="social-icon-colored">
                            <li><a href="#"><span class="fab fa-facebook-f"></span></a></li>


                        
                        </ul>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header Top -->

        <!-- Header Upper -->
        <div class="header-lower">
            <div class="container clearfix">
                <!--Info-->
                <div class="logo-outer">
                    <div class="logo"><a href="{{ url ('/home') }}"><img src="{{ asset ('img/logo.png') }}" style="width: 65%;" alt="" title=""></a></div>
                </div>

                <!--Nav Box-->
                <div class="nav-outer clearfix">
                    <!-- Main Menu -->
                    <nav class="main-menu navbar-expand-md navbar-light">
                        <div class="navbar-header">
                            <!-- Togg le Button -->      
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="icon flaticon-menu-button"></span>
                            </button>
                        </div>
                        
                        <div class="collapse navbar-collapse clearfix" id="navbarSupportedContent">
                            <ul class="navigation clearfix">
                                
                                <li><a href="{{ url ('/home') }}">Home</a></li>

                                <li class="dropdown"><a href="{{ url ('/about-us') }}">About Us</a>

                                     <ul>
                                         <li><a href="{{ asset ('img/mandatory.pdf') }}">CBSE Mandatory Disclosure</a></li>

                                        <li><a href="{{ url ('/management') }}">School Management Committe</a></li>

                                        <li><a href="{{ url ('/staffs') }}">Teaching Staffs</a></li>

                                         <li><a href="{{ url ('/students') }}">No. of Students Class Wise</a></li>

                                          <li><a href="{{ url ('/norms') }}">Norms For Fixing Fees</a></li>

                                        <li><a target="_blank" href="{{ asset ('img/textbooks.pdf') }}">Prescribed Text Books</a></li>

                                       
                                    
                                    
                                    </ul>


                                </li>

                                <li class="dropdown"><a href="{{ url ('/academics') }}">Academics</a>

                                       <ul>
                                        <li><a href="{{ url ('academics#curriculum') }}">Curriculum </a></li>
                                        <li><a href="{{ url ('academics#facilities') }}">Infrastructure </a></li>
                                        <li><a href="{{ url ('academics#library') }}">Library</a></li>
                                         <li><a href="{{ url ('academics#calendar') }}">Academic Calendar</a></li>
                                        <li><a href="{{ url ('academics#toppers') }}">Academic Toppers</a></li>
                                        <li><a href="{{ url ('/annual-report') }}">Annual Report</a></li>
                                        <li><a href="{{ url ('/circular') }}">School Circular</a></li>

                                         <li><a target="_blank" href="{{ asset ('img/tcformat.jpg') }}">TC Format</a></li>

                                        <li><a target="_blank" href="{{ asset ('img/no-homework-policy.pdf') }}">No Homework Policy</a></li>

                                        <li><a target="_blank" href="{{ asset ('img/25_circular_2018.pdf') }}">CBSE Circular</a></li>

                                       

                                
                                    </ul>


                                </li>

                                <li><a href="{{ url ('/activities') }}">Activities</a></li>

                                 <li class="dropdown"><a href="{{ url ('/beyond-academics') }}">Beyond Academics</a>

                                     <ul>
                                        <li><a href="{{ url ('beyond-academics#tours') }}">Tours and Exposures</a></li>
                                        <li><a href="{{ url ('beyond-academics#personality') }}">Overall  Personality  Development</a></li>
                                        <li><a href="{{ url ('beyond-academics#fitkids') }}">Fit Kids</a></li>

                                        <li><a href="{{ url ('beyond-academics#media') }}">Media</a></li>

                                        <li><a href="{{ url ('beyond-academics#arts') }}">Arts & Crafts</a></li>
                                
                                    </ul>


                                </li>

                             

                                   <li class="dropdown"><a href="{{ url ('/gallery') }}">Gallery</a>

                                     <ul>
                                        <li><a href="{{ url ('gallery1') }}">New Year 2021</a></li>

                                         <li><a href="{{ url ('gallery2') }}">Women's Day</a></li>
                                 
                                
                                    </ul>


                                </li>

<!-- 

                                <li class="dropdown"><a href="{{ url ('/facilities') }}">Facilities</a>

                                     <ul>
                                        <li><a href="#">Library</a></li>
                                        <li><a href="#">Playground</a></li>
                                        <li><a href="#">Dining Hall</a></li>
                                        <li><a href="#">Transportation</a></li>
                                        <li><a href="#">Trips & Excursions</a></li>
                                    </ul>


                                </li>
 -->
                                <li><a href="{{ url ('/contact-us') }}">Contact Us</a></li>
                            </ul>
                        </div>
                    </nav>
                    <!-- Main Menu End-->

                
                </div>
            </div>
        </div>
        <!--End Header Upper-->

   <!--     
        <div class="sticky-header">
            <div class="auto-container clearfix">
          
                <div class="logo pull-left">
                    <a href="{{ url ('/home') }}" title=""><img src="{{ asset ('img/logo.png') }}" style="width: 80%;" alt="" title=""></a>
                </div>
             
                <div class="pull-right">
              
                    <nav class="main-menu">
                        <div class="navbar-collapse show collapse clearfix">
                            <ul class="navigation clearfix">
                                    
                                 <li><a href="{{ url ('/home') }}">Home</a></li>

                                <li><a href="{{ url ('/about-us') }}">About Us</a></li>

                                <li><a href="{{ url ('/academics') }}">Academics</a></li>

                                <li><a href="{{ url ('/gallery') }}">Gallery</a></li>

                                <li><a href="{{ url ('/contact-us') }}">Contact Us</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div> -->
    </header>
    <!-- End Main Header -->



    @yield('content')




    <!--Main Footer-->
    <footer class="main-footer">
        <div class="anim-icons">
            <span class="icon icon-sparrow wow zoomIn" data-wow-delay="400ms"></span>
            <span class="icon icon-rainbow-2 wow zoomIn" data-wow-delay="800ms"></span>
            <span class="icon icon-star-3"></span>
            <span class="icon icon-star-3 two"></span>
            <span class="icon icon-sun"></span>
            <span class="icon icon-plane wow zoomIn" data-wow-delay="1200ms"></span>
        </div>

        <!--Scroll to top-->
        <div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon icon-arrow-up"></span></div>

        <!--footer upper-->
     <!--    <div class="footer-upper">
            <div class="auto-container">
                <div class="row clearfix">
                
                    <div class="big-column col-xl-6 col-lg-12 col-md-12 col-sm-12">
                        <div class="row clearfix">
                
                            <div class="footer-column col-lg-6 col-md-6 col-sm-12">
                                <div class="footer-widget logo-widget">
                                    <div class="logo">
                                        <a href="{{ url ('/home') }}"><img src="{{ asset ('img/logowhite.png') }}" alt="" /></a>
                                    </div>
                                    <div class="text" align="center">The school is committed to inculcating in the children good values apart from education to develop them into responsible, confident, healthy youngsters embodying good character.</div>

                                </div>
                            </div>
        
                            <div class="footer-column col-lg-6 col-md-6 col-sm-12">
                                <div class="footer-widget contact-widget">
                                    <h4 class="widget-title">Contact</h4>
                                    <div class="widget-content">
                                        <ul class="contact-info">    
                                            <li><a href="tel:6668880000"><span class="fa fa-phone-square"></span>666 888 0000</a></li>
                                            <li><a href="mailto:info@bebio.com"><span class="fa fa-envelope"></span>contact@vijyaprimaryschool.com</a></li>
                                            <li><span class="fa fa-map"></span>Vijaya Higher primary School, Pandavapura, Mandya Dist. Karnataka.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                 
                    <div class="big-column col-xl-6 col-lg-12 col-md-12 col-sm-12">
                        <div class="row clearfix">
                        
                            <div class="footer-column col-xl-3 col-lg-3 col-md-6 col-sm-4">
                                <div class="footer-widget links-widget">
                                    <h4 class="widget-title">Links</h4>
                                    <div class="widget-content">
                                        <ul class="list">
                                            <li><a href="#">About</a></li>
                                            <li><a href="#">Contact</a></li>
                                            <li><a href="#">Our Gallery</a></li>
                                            <li><a href="#">Programs</a></li>
                                            <li><a href="#">Events</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        
                            <div class="footer-column col-xl-4 col-lg-3 col-md-6 col-sm-12">
                                <div class="footer-widget activites-widget">
                                    <h4 class="widget-title">Activities</h4>
                                    <div class="widget-content">
                                        <ul class="list">
                                            <li><a href="#">Table/Floor Toys</a></li>
                                            <li><a href="#">Outdoor Games</a></li>
                                            <li><a href="#">Sand Play</a></li>
                                            <li><a href="#">Play Dough</a></li>
                                            <li><a href="#">Building Blocks</a></li>
                                            <li><a href="#">Water Play</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                    
                            <div class="footer-column col-xl-5 col-lg-6 col-md-12 col-sm-12">
                                <div class="footer-widget newsletter-widget">
                                    <h4 class="widget-title">Newsletter</h4>
                                    <div class="newsletter-form">
                                        <form method="post" action="http://t.commonsupport.xyz/bebio/contact.html">
                                            <div class="form-group">
                                                <input type="email" name="email" value="" placeholder="Enter Your Email" required="">
                                                <button type="submit" class="theme-btn btn-style-one">Subscribe</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!--Footer Bottom-->
        <div class="footer-bottom">
            <div class="auto-container">
             
                <div class="copyright"> Copyrights &copy; 2020 Vijaya Higher Primary School. powered By <a target="_blank" href="https://mdbstech.com/">MDBS Tech Pvt. ltd.</a></div>
            </div>
        </div>
    </footer>
    <!--End Main Footer-->

</div>
<!--End pagewrapper-->





<script src="{{ asset ('js/jquery.js') }}"></script>
<script src="{{ asset ('js/popper.min.js') }}"></script>
<script src="{{ asset ('js/bootstrap.min.js') }}"></script>
<!--Revolution Slider-->
<script src="{{ asset ('plugins/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ asset ('plugins/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ asset ('plugins/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
<script src="{{ asset ('plugins/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
<script src="{{ asset ('plugins/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
<script src="{{ asset ('plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
<script src="{{ asset ('plugins/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
<script src="{{ asset ('plugins/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
<script src="{{ asset ('plugins/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
<script src="{{ asset ('plugins/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
<script src="{{ asset ('plugins/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
<script src="{{ asset ('js/main-slider-script.js') }}"></script>
<!--Revolution Slider-->
<script src="{{ asset ('js/jquery-ui.js') }}"></script>
<script src="{{ asset ('js/jquery.fancybox.js') }}"></script>
<script src="{{ asset ('js/owl.js') }}"></script>
<script src="{{ asset ('js/jquery.countdown.js') }}"></script>
<script src="{{ asset ('js/wow.js') }}"></script>
<script src="{{ asset ('js/appear.js') }}"></script>
<script src="{{ asset ('js/parallax.min.js') }}"></script>
<script src="{{ asset ('js/script.js') }}"></script>
<!--Google Map APi Key-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDcaOOcFcQ0hoTqANKZYz-0ii-J0aUoHjk"></script>
<script src="{{ asset ('js/map-script.js') }}"></script>
<!--End Google Map APi-->
</body>

</html>