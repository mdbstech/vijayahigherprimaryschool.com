@extends('layouts.appfront')

@section('content')

   <!--Page Title-->
    <section class="page-title" style="background-image:url(img/banner.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
              
                <h1>Annual Report</h1>
            </div>
        </div>
    </section>
    <!--End Page Title-->


                <!-- About Section Two -->
    <section class="about-section">
        <div class="auto-container">
        

        

            <div class="row">
                <!-- Content Column -->
                <div class="content-column col-lg-12 col-md-12 col-sm-12 order-2">
                    <div class="inner-column">
                       
                       <ul class="list-style-one"> 
                     
                   

                   		 <li style="color: black;">	The Academic Year 2019-20 Started On 27-05-2019. </li>
						 <li style="color: black;">	June 5th 2019 We Celebrated Environmental Day In Our School by delivering the speech about importance of Environment.</li> 
						 <li style="color: black;">	We Conducted Parents Teachers Meeting (PTM) On 06/07/2019 Subjected To Discuss About Educating The Students And Disciplines Of The School Which Should Be Followed By The Students. </li>
						 <li style="color: black;">	Our Students Has Participated In “Prathiba Karanji” And Won By 13 Competition Out Of 18 Competition.</li>
						 <li style="color: black;">	We Celebrated Independence Day On 15/08/2019 In School Premises by conducting Fancy Dress (freedom fighters) Competition.</li>
						 <li style="color: black;">	We Participated In DRAWING COMPETION Which Is Organised By “SRI KALANIKETHAN SCHOOL OF ARTS, MYSORE” AND won BY 1ST PRIZE IN DISTRICT Level.</li>
						 <li style="color: black;">	 We Celebrated “Raksha Bandan” On 16/08/2019 In our School Premises.</li>
						 <li style="color: black;">	We Celebrated “KANNADA RAJYOTHSAVA” ON 01/11/2019 in the school.</li>
						 <li style="color: black;">	We organised “SCIENCE EXHIBITION” On 13/11/2019 in our school premises. </li>
						 <li style="color: black;">	We Organised Essay Competition , Dance Competition, Singing Competition, Memory Test Competition, Quiz Competition Drawing Competition In Our School For Annual Day Function AND We Celebrated Annual Day As A “KIDS GLITTERING FEST-2K20” On 14/02/2020.</li>


                  </ul>
                      
                    </div>
                </div>

              
            </div>
        </div>
    </section>
    <!--End About Section Two -->

@endsection
