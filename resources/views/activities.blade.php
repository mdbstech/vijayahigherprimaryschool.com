@extends('layouts.appfront')

@section('content')

  <!--Page Title-->
    <section class="page-title" style="background-image:url(img/banner.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
              
                <h1>Activities</h1>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Program Section -->
    <section class="program-section alternate">
        <div class="auto-container">
            <div class="row">
                <!-- Program Block -->
                <div class="program-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/sports.jpg') }}" style="height:400px;" alt=""></figure>
                        </div>
                        <div class="lower-content">
                            <h4>Sports and Physical Fitness</h4>
                            <div class="text" align="justify">Emphasis is given to make  children physically and mentally strong  .Sports, Exercise and team games are  organised to encourage team spiri. Sports Day gives the children opportunities to  exhibit their prowess in sports and prizes are distributed to motivate them.</div>
                           
                        </div>
                    </div>
                </div>

                <!-- Program Block -->
                <div class="program-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="400ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/health.jpg') }}" style="height:400px;" alt=""></figure>
                        </div>
                        <div class="lower-content">
                            <h4>Regular Health  Checkups</h4>
                            <div class="text" align="justify">The children go through regular health check ups by a panel of qualified doctors to identify health deficiencies and medication and corrective measures are prescribed.</div>
                           
                        </div>
                    </div>
                </div>

                <!-- Program Block -->
                <div class="program-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="800ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/tour.jpg') }}" style="height:400px;" alt=""></figure>
                        </div>
                        <div class="lower-content">
                            <h4>Days of National Importance</h4>
                            <div class="text" align="justify">Important days like Independence Day, Republic Day , Gandhi Jayanthi are celebrated to inculcate in children respect for our country, National Flag, National Anthem and to remember the   sacrifices of our great leaders who got freedom for our country.Role plays and small skits are enacted by the children to make these days memorable and interesting.</div>
                           
                        </div>
                    </div>
                </div>

                <!-- Program Block -->
                <div class="program-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/festival.jpg') }}" style="height:400px;" alt=""></figure>
                        </div>
                        <div class="lower-content">
                            <h4>Celebration of important festivals</h4>
                            <div class="text" align="justify">To foster respect among children for all the religions important festivals like Ganesh Chathurthi, Navrathri, Deepavali, Christmas,Eid ,Mahaveer Jayanthi, Budha Poornima are celebrated with pomp and traditional fervor.</div>
                           
                        </div>
                    </div>
                </div>

                <!-- Program Block -->
                <div class="program-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="400ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/act3.jpg') }}" style="height:400px;" alt=""></figure>
                        </div>
                        <div class="lower-content">
                            <h4>Celebration of birthdays of prominent leaders and personalities</h4>
                            <div class="text" align="justify"> Gandhi Jayanthi, Ambedkar Jaynthi, Basava Jayanthi, Kanakadasa Jayanthi are celebrated to remind the children of their great contribution to the culture and progress of India.</div>
                           
                        </div>
                    </div>
                </div>

                <!-- Program Block -->
                <div class="program-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="800ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/gallery/g24.jpg') }}" style="height:400px;" alt=""></figure>
                        </div>
                        <div class="lower-content">
                            <h4>Cultural function</h4>
                            <div class="text" align="justify">To bring out the talent in children and to encourage team work children put up cultural shows with Music, Drama, Dance etc to  exhibit their talents and entertain their friends and parents.Attractive prizes are given to the individual children and best performing teams.November 14th of every year (Nehru Jayanthi) is observed as Children’s Day and children put up memorable performances.During these events, the anchoring and stage management are taken care of by children.</div>
                           
                        </div>
                    </div>
                </div>


                 <!-- Program Block -->
                <div class="program-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="800ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/act2.jpg') }}" style="height:400px;" alt=""></figure>
                        </div>
                        <div class="lower-content">
                            <h4>Essay writing, Poetry and Elocution Competitions</h4>
                            <div class="text" align="justify">To bring out the talent in children and to encourage team work children put up cultural shows with Music, Drama, Dance etc to  exhibit their talents and entertain their friends and parents.Attractive prizes are given to the individual children and best performing teams.November 14th of every year (Nehru Jayanthi) is observed as Children’s Day and children put up memorable performances.During these events, the anchoring and stage management are taken care of by children.</div>
                           
                        </div>
                    </div>
                </div>

                 <!-- Program Block -->
                <div class="program-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="800ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/teachers.jpg') }}" style="height:400px;" alt=""></figure>
                        </div>
                        <div class="lower-content">
                            <h4>Teachers’ Day Celebration </h4>
                            <div class="text" align="justify">To bring out the talent in children and to encourage team work children put up cultural shows with Music, Drama, Dance etc to  exhibit their talents and entertain their friends and parents.Attractive prizes are given to the individual children and best performing teams.November 14th of every year (Nehru Jayanthi) is observed as Children’s Day and children put up memorable performances.During these events, the anchoring and stage management are taken care of by children.</div>
                           
                        </div>
                    </div>
                </div>

                 <!-- Program Block -->
                <div class="program-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="800ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/act1.jpg') }}" style="height:400px;" alt=""></figure>
                        </div>
                        <div class="lower-content">
                            <h4>Educational visits</h4>
                            <div class="text" align="justify">To bring out the talent in children and to encourage team work children put up cultural shows with Music, Drama, Dance etc to  exhibit their talents and entertain their friends and parents.Attractive prizes are given to the individual children and best performing teams.November 14th of every year (Nehru Jayanthi) is observed as Children’s Day and children put up memorable performances. During these events, the anchoring and stage management are taken care of by children.</div>
                           
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--End Program Section -->

@endsection