@extends('layouts.appfront')

@section('content')

  <!--Page Title-->
    <section class="page-title" style="background-image:url(img/banner.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
              
                <h1>Beyond Academics</h1>
            </div>
        </div>
    </section>
    <!--End Page Title-->
 <!-- Program Detail -->
    <section class="program-detail" id="tours">
        <div class="auto-container">
            <div class="row">
                <!-- Content Column -->
                <div class="content-column col-lg-6 col-md-12 col-sm-12 order-2">
                    <div class="inner-column">
                        <h3>Tours and Exposures</h3>
                        <div class="text" align="justify">Travelling is a important part of education. It gives us practical knowledge. We learn lots of lessons from it. Through travelling we come across new things and new places. Educational trips gives us knowledge, experience and entertainment as well.</div>
						<div class="text" align="justify">Educational trips are arranged by our school . Last year an educational trip was arranged by my school. I also joined it.We hired a luxury bus from Tour and travels agency. l had lots of fun.
						</div>
                    </div>
                </div>

                <!-- Image Column -->
                <div class="image-column col-lg-6 col-md-12 col-sm-12">
                    <div class="image-box wow fadeIn">
                        <figure class="image"><a href="{{ asset ('img/tour.jpg')}}" class="lightbox-image"><img src="{{ asset ('img/tour.jpg')}}" alt=""></a></figure>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Program Detail -->

     <!-- Program Detail -->
    <section class="program-detail" id="personality">
        <div class="auto-container">
            <div class="row">

            	    <!-- Image Column -->
                <div class="image-column col-lg-6 col-md-12 col-sm-12 order-2 d-none d-sm-block">
                    <div class="image-box wow fadeIn">
                        <figure class="image"><a href="{{ asset ('img/h1.jpg')}}" class="lightbox-image"><img src="{{ asset ('img/h1.jpg')}}" alt=""></a></figure>
                    </div>
                </div>


                <!-- Content Column -->
                <div class="content-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-column">
                        <h3>Overall  Personality  Development</h3>
                        <div class="text" align="justify"><b>Sports</b> are very important for developing healthy and strong body. Sports play great role in improving and maintaining the health and fitness, improving mental skills and concentration level. </div>
						<div class="text" align="justify">Vijaya higher Primary School arrange for ‘Annual Sports’ once every year. The annual sports day of our school was held on 14th November. On this occasion, our school building and surroundings were nicely decorated with flowers, colour-papers and balloons. The Secretary was the chief guest. He hoisted the national flag with the national anthem.
						</div>
                                       </div>
                </div>

            
            </div>
        </div>
    </section>
    <!--End Program Detail -->

     <!-- Program Detail -->
    <section class="program-detail" id="fitkids">
        <div class="auto-container">
            <div class="row">
                <!-- Content Column -->
                <div class="content-column col-lg-6 col-md-12 col-sm-12 order-2">
                    <div class="inner-column">
                        <h3>Fit Kids</h3>
                        <div class="text" align="justify">Generally a month of preparation precedes the day. A month prior to the event, all the children of the school were divided into four houses and respective sports groups such as Primary classes and secondary classes.</div>
						<div class="text" align="justify">The students took part in various games. Main items of sports were high jump, long jump, short put, javelin throw, various races and disk throwing. The participating students display their ability through these sports. Some team events were also organized like Kabaddi, football, volley ball and tug of war.  Our P. T. teacher conducted the entire show. He was assisted by three other teachers. The events closed with a prize giving ceremony. The chief guest gave the prizes to the winner. It was really a day of great joy for us.
						</div>
                                       </div>
                </div>

                <!-- Image Column -->
                <div class="image-column col-lg-6 col-md-12 col-sm-12">
                    <div class="image-box wow fadeIn">
                        <figure class="image"><a href="{{ asset ('img/yoga.jpg')}}" class="lightbox-image"><img src="{{ asset ('img/yoga.jpg')}}" alt=""></a></figure>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Program Detail -->

         <!-- Program Detail -->
    <section class="program-detail" id="media">
        <div class="auto-container">
            <div class="row">

            	    <!-- Image Column -->
                <div class="image-column col-lg-6 col-md-12 col-sm-12 order-2">
                    <div class="image-box wow fadeIn">
                        <figure class="image"><a href="{{ asset ('img/media.jpg')}}" class="lightbox-image"><img src="{{ asset ('img/media.jpg')}}" alt=""></a></figure>
                    </div>
                </div>


                <!-- Content Column -->
                <div class="content-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-column">
                        <h3>Media</h3>
                        <div class="text" align="justify">VHPS PROVIDES Computer a few Skills of the most common software programs and technical skills </div>
					
                                       </div>
                </div>

            
            </div>
        </div>
    </section>
    <!--End Program Detail -->	

        <!-- Program Detail -->
    <section class="program-detail" id="arts">
        <div class="auto-container">
            <div class="row">
                <!-- Content Column -->
                <div class="content-column col-lg-6 col-md-12 col-sm-12 order-2">
                    <div class="inner-column">
                        <h3>Arts & Crafts</h3>
                        <div class="text" align="justify">VHPS  ensures a wide variety of activities involving making things with one's own hands. Arts and crafts is usually a hobby. Some crafts (art skills) have been practised for centuries, others are more recent inventions. A quote by Apoorva Rathore: Art cannot be taught, it comes from thinking. But it can be improved by practice.</div>
						<div class="text" align="justify">Both children and adults enjoy arts and crafts. Children in  our schools may learn skills such as woodworking, wood carving, sewing, or making things with all sorts of material. Many community centres and schools have evening or day classes and workshops where one can learn arts and craft skills.
						</div>
                                       </div>
                </div>

                <!-- Image Column -->
                <div class="image-column col-lg-6 col-md-12 col-sm-12">
                    <div class="image-box wow fadeIn">
                        <figure class="image"><a href="{{ asset ('img/crafts.jpg')}}" class="lightbox-image"><img src="{{ asset ('img/crafts.jpg')}}" alt=""></a></figure>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Program Detail -->

@endsection