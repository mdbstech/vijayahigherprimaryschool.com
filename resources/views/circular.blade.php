@extends('layouts.appfront')

@section('content')

   <!--Page Title-->
    <section class="page-title" style="background-image:url(img/banner.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
              
                <h1>School Circular</h1>
            </div>
        </div>
    </section>
    <!--End Page Title-->


                <!-- About Section Two -->
    <section class="about-section">
        <div class="auto-container">
        

        

            <div class="row">
                <!-- Content Column -->
                <div class="content-column col-lg-10 col-md-12 col-sm-12 order-2">
                    <div class="inner-column">
                       
                       <ul class="list-style-one"> 
                 
                       <li style="color: black;"> Parents Teachers meeting (PTM) will be conducted on 06/07/2019 subjected to discuss about educating the students and disciplines of the school which should be followed by the students.</li>
                       <li style="color: black;"> Conducting Formative Assessment-I from 29/072019 to 05/08/2019</li>
                       <li style="color: black;"> Celebration of Independence Day on 15/08/2019 in school premises.</li>
                       <li style="color: black;"> Celebration of Raksha Bandhan on 16/08/2019 in school.</li>
                       <li style="color: black;"> Conducting Formative Assessment-II from 30/08/2019 to 07/08/2019</li>
                       <li style="color: black;"> Conducting SUMMATIVE ASSESSMET –I from 23/09/2019 TO 04/10/2019</li>
                       <li style="color: black;"> Dasara Holidays starts from 05/10/2019 to 21/10/2019</li>
                       <li style="color: black;"> Celabration of KANNADA RAJYOTHSAVA ON 01/11/2019</li>
                       <li style="color: black;"> Science Exhibition will be conducted on 13/11/2019</li>
                       <li style="color: black;"> Conducting Formative Assessment-III from 07/12/2019  to 11/12/2019</li>
                       <li style="color: black;"> Conducting Formative Assessment-IV from 01/03/2019 to 26/01/2020  to 30/01/2020  </li> 
                       <li style="color: black;"> Celebrating annual day as a “KIDS GLITTERING FEST-2K20”</li>
                       <li style="color: black;"> Conducting SUMMATIVE ASSESSMENT II from 14/03/2020 to 23/03/2020</li>


                  </ul>
                      
                    </div>
                </div>

              
            </div>
        </div>
    </section>
    <!--End About Section Two -->

@endsection
