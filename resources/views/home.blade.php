                  @extends('layouts.appfront')

                  @section('content')


                   <!--Main Slider-->
                      <section class="main-slider style-two">
                          <div class="rev_slider_wrapper fullwidthbanner-container"  id="rev_slider_one_wrapper" data-source="gallery">
                              <div class="rev_slider fullwidthabanner" id="rev_slider_one" data-version="5.4.1">
                                  <ul>
                                      <!-- Slide 1 -->
                                      <li data-transition="parallaxvertical" data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1681" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="{{ asset ('img/h1.jpg') }}">

                                          <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="{{ asset ('img/h10.jpg') }}">
                                          
                                          <div class="tp-caption tp-resizeme big-ipad-hidden tp-shape tp-shapewrapper rs-parallaxlevel-4" 
                                          data-paddingbottom="[0,0,0,0]"
                                          data-paddingleft="[0,0,0,0]"
                                          data-paddingright="[0,0,0,0]"
                                          data-paddingtop="[0,0,0,0]"
                                          data-responsive_offset="on"
                                          data-type="text"
                                          data-height="none"
                                          data-whitespace="nowrap"
                                          data-width="none"
                                          data-hoffset="['-80','15','15','15']"
                                          data-voffset="['30','0','0','0']"
                                          data-x="['left','left','left','left']"
                                          data-y="['top','top','top','top']"
                                          data-textalign="['top','top','top','top']"
                                          data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                                              <span class="icon icon-sun-2"></span>
                                          </div>


                                          <div class="tp-caption tp-resizeme big-ipad-hidden tp-shape tp-shapewrapper rs-parallaxlevel-3" 
                                          data-paddingbottom="[0,0,0,0]"
                                          data-paddingleft="[0,0,0,0]"
                                          data-paddingright="[0,0,0,0]"
                                          data-paddingtop="[0,0,0,0]"
                                          data-responsive_offset="on"
                                          data-type="text"
                                          data-height="none"
                                          data-whitespace="nowrap"
                                          data-width="none"
                                          data-hoffset="['-255','15','15','15']"
                                          data-voffset="['175','0','0','0']"
                                          data-x="['left','left','left','left']"
                                          data-y="['top','top','top','top']"
                                          data-textalign="['top','top','top','top']"
                                          data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                                              <span class="icon icon-star"></span>
                                          </div>

                                          <div class="tp-caption tp-resizeme big-ipad-hidden tp-shape tp-shapewrapper rs-parallaxlevel-3" 
                                          data-paddingbottom="[0,0,0,0]"
                                          data-paddingleft="[0,0,0,0]"
                                          data-paddingright="[0,0,0,0]"
                                          data-paddingtop="[0,0,0,0]"
                                          data-responsive_offset="on"
                                          data-type="text"
                                          data-height="none"
                                          data-whitespace="nowrap"
                                          data-width="none"
                                          data-hoffset="['440','15','15','15']"
                                          data-voffset="['105','0','0','0']"
                                          data-x="['left','left','left','left']"
                                          data-y="['top','top','top','top']"
                                          data-textalign="['top','top','top','top']"
                                          data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                                              <span class="icon icon-star-4"></span>
                                          </div>

                                          <div class="tp-caption tp-resizeme big-ipad-hidden tp-shape tp-shapewrapper rs-parallaxlevel-4" 
                                          data-paddingbottom="[0,0,0,0]"
                                          data-paddingleft="[0,0,0,0]"
                                          data-paddingright="[0,0,0,0]"
                                          data-paddingtop="[0,0,0,0]"
                                          data-responsive_offset="on"
                                          data-type="text"
                                          data-height="none"
                                          data-whitespace="nowrap"
                                          data-width="none"
                                          data-hoffset="['150','15','15','15']"
                                          data-voffset="['40','0','0','0']"
                                          data-x="['right','right','right','right']"
                                          data-y="['top','top','top','top']"
                                          data-textalign="['top','top','top','top']"
                                          data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                                              <span class="icon icon-idea"></span>
                                          </div>

                                          <div class="tp-caption tp-resizeme big-ipad-hidden tp-shape tp-shapewrapper rs-parallaxlevel-5" 
                                          data-paddingbottom="[0,0,0,0]"
                                          data-paddingleft="[0,0,0,0]"
                                          data-paddingright="[0,0,0,0]"
                                          data-paddingtop="[0,0,0,0]"
                                          data-responsive_offset="on"
                                          data-type="text"
                                          data-height="none"
                                          data-whitespace="nowrap"
                                          data-width="none"
                                          data-hoffset="['-360','15','15','15']"
                                          data-voffset="['150','0','0','0']"
                                          data-x="['left','left','left','left']"
                                          data-y="['bottom','bottom','bottom','bottom']"
                                          data-textalign="['top','top','top','top']"
                                          data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                                              <span class="icon icon-pencil"></span>
                                          </div>

                                          <div class="tp-caption" 
                                          data-paddingbottom="[0,0,0,0]"
                                          data-paddingleft="[0,0,0,0]"
                                          data-paddingright="[0,0,0,0]"
                                          data-paddingtop="[0,0,0,0]"
                                          data-responsive_offset="on"
                                          data-type="text"
                                          data-height="none"
                                          data-width="['650','650','670','400']"
                                          data-whitespace="normal"
                                          data-hoffset="['15','15','15','15']"
                                          data-voffset="['-185','-180','-140','-210']"
                                          data-x="['left','left','left','left']"
                                          data-y="['middle','middle','middle','middle']"
                                          data-textalign="['top','top','top','top']"
                                          data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                                              <div class="title">Vidya Prachara Sangha (R)</div>
                                          </div>

                                          <div class="tp-caption" 
                                          data-paddingbottom="[0,0,0,0]"
                                          data-paddingleft="[0,0,0,0]"
                                          data-paddingright="[0,0,0,0]"
                                          data-paddingtop="[0,0,0,0]"
                                          data-responsive_offset="on"
                                          data-type="text"
                                          data-height="none"
                                          data-width="['650','730','650','400']"
                                          data-whitespace="normal"
                                          data-hoffset="['15','15','15','15']"
                                          data-voffset="['-25','-25','-25','-25']"
                                          data-x="['left','left','left','left']"
                                          data-y="['middle','middle','middle','middle']"
                                          data-textalign="['top','top','top','top']"
                                          data-frames='[{"delay":1500,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                                              <h2>Your Success….. Our Mission….</h2>
                                          </div>
                                          
                                    
                                      </li>

                                      <!-- Slide 2 -->
                                      <li data-transition="parallaxvertical" data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1682" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="{{ asset ('img/h7.jpg') }}">

                                          <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="{{ asset ('img/h1.jpg') }}">
                                          
                                          <div class="tp-caption tp-resizeme big-ipad-hidden tp-shape tp-shapewrapper rs-parallaxlevel-4" 
                                          data-paddingbottom="[0,0,0,0]"
                                          data-paddingleft="[0,0,0,0]"
                                          data-paddingright="[0,0,0,0]"
                                          data-paddingtop="[0,0,0,0]"
                                          data-responsive_offset="on"
                                          data-type="text"
                                          data-height="none"
                                          data-whitespace="nowrap"
                                          data-width="none"
                                          data-hoffset="['-80','15','15','15']"
                                          data-voffset="['30','0','0','0']"
                                          data-x="['left','left','left','left']"
                                          data-y="['top','top','top','top']"
                                          data-textalign="['top','top','top','top']"
                                          data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                                              <span class="icon icon-sun-2"></span>
                                          </div>


                                          <div class="tp-caption tp-resizeme big-ipad-hidden tp-shape tp-shapewrapper rs-parallaxlevel-3" 
                                          data-paddingbottom="[0,0,0,0]"
                                          data-paddingleft="[0,0,0,0]"
                                          data-paddingright="[0,0,0,0]"
                                          data-paddingtop="[0,0,0,0]"
                                          data-responsive_offset="on"
                                          data-type="text"
                                          data-height="none"
                                          data-whitespace="nowrap"
                                          data-width="none"
                                          data-hoffset="['-255','15','15','15']"
                                          data-voffset="['175','0','0','0']"
                                          data-x="['left','left','left','left']"
                                          data-y="['top','top','top','top']"
                                          data-textalign="['top','top','top','top']"
                                          data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                                              <span class="icon icon-star"></span>
                                          </div>

                                          <div class="tp-caption tp-resizeme big-ipad-hidden tp-shape tp-shapewrapper rs-parallaxlevel-3" 
                                          data-paddingbottom="[0,0,0,0]"
                                          data-paddingleft="[0,0,0,0]"
                                          data-paddingright="[0,0,0,0]"
                                          data-paddingtop="[0,0,0,0]"
                                          data-responsive_offset="on"
                                          data-type="text"
                                          data-height="none"
                                          data-whitespace="nowrap"
                                          data-width="none"
                                          data-hoffset="['440','15','15','15']"
                                          data-voffset="['105','0','0','0']"
                                          data-x="['left','left','left','left']"
                                          data-y="['top','top','top','top']"
                                          data-textalign="['top','top','top','top']"
                                          data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                                              <span class="icon icon-star-4"></span>
                                          </div>

                                          <div class="tp-caption tp-resizeme big-ipad-hidden tp-shape tp-shapewrapper rs-parallaxlevel-4" 
                                          data-paddingbottom="[0,0,0,0]"
                                          data-paddingleft="[0,0,0,0]"
                                          data-paddingright="[0,0,0,0]"
                                          data-paddingtop="[0,0,0,0]"
                                          data-responsive_offset="on"
                                          data-type="text"
                                          data-height="none"
                                          data-whitespace="nowrap"
                                          data-width="none"
                                          data-hoffset="['150','15','15','15']"
                                          data-voffset="['40','0','0','0']"
                                          data-x="['right','right','right','right']"
                                          data-y="['top','top','top','top']"
                                          data-textalign="['top','top','top','top']"
                                          data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                                              <span class="icon icon-idea"></span>
                                          </div>

                                          <div class="tp-caption tp-resizeme big-ipad-hidden tp-shape tp-shapewrapper rs-parallaxlevel-5" 
                                          data-paddingbottom="[0,0,0,0]"
                                          data-paddingleft="[0,0,0,0]"
                                          data-paddingright="[0,0,0,0]"
                                          data-paddingtop="[0,0,0,0]"
                                          data-responsive_offset="on"
                                          data-type="text"
                                          data-height="none"
                                          data-whitespace="nowrap"
                                          data-width="none"
                                          data-hoffset="['-360','15','15','15']"
                                          data-voffset="['150','0','0','0']"
                                          data-x="['left','left','left','left']"
                                          data-y="['bottom','bottom','bottom','bottom']"
                                          data-textalign="['top','top','top','top']"
                                          data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                                              <span class="icon icon-pencil"></span>
                                          </div>

                                          <div class="tp-caption" 
                                          data-paddingbottom="[0,0,0,0]"
                                          data-paddingleft="[0,0,0,0]"
                                          data-paddingright="[0,0,0,0]"
                                          data-paddingtop="[0,0,0,0]"
                                          data-responsive_offset="on"
                                          data-type="text"
                                          data-height="none"
                                          data-width="['650','650','670','400']"
                                          data-whitespace="normal"
                                          data-hoffset="['15','15','15','15']"
                                          data-voffset="['-185','-180','-140','-210']"
                                          data-x="['left','left','left','left']"
                                          data-y="['middle','middle','middle','middle']"
                                          data-textalign="['top','top','top','top']"
                                          data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                                              <div class="title">Vidya Prachara Sangha (R)</div>
                                          </div>

                                          <div class="tp-caption" 
                                          data-paddingbottom="[0,0,0,0]"
                                          data-paddingleft="[0,0,0,0]"
                                          data-paddingright="[0,0,0,0]"
                                          data-paddingtop="[0,0,0,0]"
                                          data-responsive_offset="on"
                                          data-type="text"
                                          data-height="none"
                                          data-width="['650','730','650','400']"
                                          data-whitespace="normal"
                                          data-hoffset="['15','15','15','15']"
                                          data-voffset="['-25','-25','-25','-25']"
                                          data-x="['left','left','left','left']"
                                          data-y="['middle','middle','middle','middle']"
                                          data-textalign="['top','top','top','top']"
                                          data-frames='[{"delay":1500,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                                              <h2>Think Creatively</h2>
                                          </div>
                             
                                      </li>

                                        <!-- Slide 2 -->
                                      <li data-transition="parallaxvertical" data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1682" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="{{ asset ('img/h2.jpg') }}">

                                          <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="{{ asset ('img/h7.jpg') }}">
                                          
                                          <div class="tp-caption tp-resizeme big-ipad-hidden tp-shape tp-shapewrapper rs-parallaxlevel-4" 
                                          data-paddingbottom="[0,0,0,0]"
                                          data-paddingleft="[0,0,0,0]"
                                          data-paddingright="[0,0,0,0]"
                                          data-paddingtop="[0,0,0,0]"
                                          data-responsive_offset="on"
                                          data-type="text"
                                          data-height="none"
                                          data-whitespace="nowrap"
                                          data-width="none"
                                          data-hoffset="['-80','15','15','15']"
                                          data-voffset="['30','0','0','0']"
                                          data-x="['left','left','left','left']"
                                          data-y="['top','top','top','top']"
                                          data-textalign="['top','top','top','top']"
                                          data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                                              <span class="icon icon-sun-2"></span>
                                          </div>


                                          <div class="tp-caption tp-resizeme big-ipad-hidden tp-shape tp-shapewrapper rs-parallaxlevel-3" 
                                          data-paddingbottom="[0,0,0,0]"
                                          data-paddingleft="[0,0,0,0]"
                                          data-paddingright="[0,0,0,0]"
                                          data-paddingtop="[0,0,0,0]"
                                          data-responsive_offset="on"
                                          data-type="text"
                                          data-height="none"
                                          data-whitespace="nowrap"
                                          data-width="none"
                                          data-hoffset="['-255','15','15','15']"
                                          data-voffset="['175','0','0','0']"
                                          data-x="['left','left','left','left']"
                                          data-y="['top','top','top','top']"
                                          data-textalign="['top','top','top','top']"
                                          data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                                              <span class="icon icon-star"></span>
                                          </div>

                                          <div class="tp-caption tp-resizeme big-ipad-hidden tp-shape tp-shapewrapper rs-parallaxlevel-3" 
                                          data-paddingbottom="[0,0,0,0]"
                                          data-paddingleft="[0,0,0,0]"
                                          data-paddingright="[0,0,0,0]"
                                          data-paddingtop="[0,0,0,0]"
                                          data-responsive_offset="on"
                                          data-type="text"
                                          data-height="none"
                                          data-whitespace="nowrap"
                                          data-width="none"
                                          data-hoffset="['440','15','15','15']"
                                          data-voffset="['105','0','0','0']"
                                          data-x="['left','left','left','left']"
                                          data-y="['top','top','top','top']"
                                          data-textalign="['top','top','top','top']"
                                          data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                                              <span class="icon icon-star-4"></span>
                                          </div>

                                          <div class="tp-caption tp-resizeme big-ipad-hidden tp-shape tp-shapewrapper rs-parallaxlevel-4" 
                                          data-paddingbottom="[0,0,0,0]"
                                          data-paddingleft="[0,0,0,0]"
                                          data-paddingright="[0,0,0,0]"
                                          data-paddingtop="[0,0,0,0]"
                                          data-responsive_offset="on"
                                          data-type="text"
                                          data-height="none"
                                          data-whitespace="nowrap"
                                          data-width="none"
                                          data-hoffset="['150','15','15','15']"
                                          data-voffset="['40','0','0','0']"
                                          data-x="['right','right','right','right']"
                                          data-y="['top','top','top','top']"
                                          data-textalign="['top','top','top','top']"
                                          data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                                              <span class="icon icon-idea"></span>
                                          </div>

                                          <div class="tp-caption tp-resizeme big-ipad-hidden tp-shape tp-shapewrapper rs-parallaxlevel-5" 
                                          data-paddingbottom="[0,0,0,0]"
                                          data-paddingleft="[0,0,0,0]"
                                          data-paddingright="[0,0,0,0]"
                                          data-paddingtop="[0,0,0,0]"
                                          data-responsive_offset="on"
                                          data-type="text"
                                          data-height="none"
                                          data-whitespace="nowrap"
                                          data-width="none"
                                          data-hoffset="['-360','15','15','15']"
                                          data-voffset="['150','0','0','0']"
                                          data-x="['left','left','left','left']"
                                          data-y="['bottom','bottom','bottom','bottom']"
                                          data-textalign="['top','top','top','top']"
                                          data-frames='[{"delay":500,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                                              <span class="icon icon-pencil"></span>
                                          </div>

                                               <div class="tp-caption" 
                                          data-paddingbottom="[0,0,0,0]"
                                          data-paddingleft="[0,0,0,0]"
                                          data-paddingright="[0,0,0,0]"
                                          data-paddingtop="[0,0,0,0]"
                                          data-responsive_offset="on"
                                          data-type="text"
                                          data-height="none"
                                          data-width="['650','650','670','400']"
                                          data-whitespace="normal"
                                          data-hoffset="['15','15','15','15']"
                                          data-voffset="['-185','-180','-140','-210']"
                                          data-x="['left','left','left','left']"
                                          data-y="['middle','middle','middle','middle']"
                                          data-textalign="['top','top','top','top']"
                                          data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                                              <div class="title">Vidya Prachara Sangha (R)</div>
                                          </div>

                                          <div class="tp-caption" 
                                          data-paddingbottom="[0,0,0,0]"
                                          data-paddingleft="[0,0,0,0]"
                                          data-paddingright="[0,0,0,0]"
                                          data-paddingtop="[0,0,0,0]"
                                          data-responsive_offset="on"
                                          data-type="text"
                                          data-height="none"
                                          data-width="['650','730','650','400']"
                                          data-whitespace="normal"
                                          data-hoffset="['15','15','15','15']"
                                          data-voffset="['-25','-25','-25','-25']"
                                          data-x="['left','left','left','left']"
                                          data-y="['middle','middle','middle','middle']"
                                          data-textalign="['top','top','top','top']"
                                          data-frames='[{"delay":1500,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                                              <h2>Your Recreational Activities starts here.</h2>
                                          </div>
                                    </li>


                                  </ul>
                              </div>
                          </div>

                          <div class="scroll-to-top scroll-to-target" data-target="#about-section"><span class="icon icon-arrow-up"></span></div>
                      </section>
                      <!--End Main Slider-->

                       <!-- About Section Two -->
                      <section class="about-section">
                          <div class="auto-container">
                              <div class="parallax-scene parallax-scene-1 anim-icons">
                                  <span data-depth="0.20" class="parallax-layer icon icon-rainbow-3"></span>
                                  <span data-depth="0.40" class="parallax-layer icon icon-star-2"></span>
                                  <span data-depth="0.30" class="parallax-layer icon icon-star-5"></span>
                           
                              </div>

                              <div class="sec-title text-center">
                                  <span class="title">Vidya Prachara Sangha (R)</span>
                                  <h2>Welcome to Vijaya Kids &<br>Higher Primary School</h2>
                              </div>

                              <div class="row">
                                  <!-- Content Column -->
                                  <div class="content-column col-lg-6 col-md-12 col-sm-12 order-2">
                                      <div class="inner-column">
                                      	<h5>Value Based Education and Activities at Vijaya Kids Care :</h5>
                                          <div class="text" align="justify">Vidya Prachara Sangha Reg started in the year 1946 before 
                                          independence. The aims and objective of the school is to provide quality education to 
                                          the rural children. It was the brain child of the 15 philanthropist who have 
                                          successfully contributed to the establishment of the institution.

                                                <br>
                                          The school is committed to inculcating in the children good values apart from education to develop them into responsible, confident, healthy youngsters embodying good character.</div>
                                          <ul class="list-style-two">
                                              <li>Supporting the Child’s Personality</li>
                                              <li>Indoor/Outdoor Games for Little Kids</li>
                                              <li>Professional & Qualified Teachers</li>
                                              <li>Best Learning School for Kids</li>
                                          </ul>
                                          <div class="btn-box"><a href="{{ url ('/about-us') }}" class="theme-btn btn-style-one">Read More</a></div>
                                      </div>
                                  </div>

                                     <div class="image-column col-lg-6 col-md-12 col-sm-12">
                                      <div class="inner-column">
                                          <figure class="image wow fadeInRight"><img src="{{ asset ('img/h4.jpg') }}" alt=""></figure>
                                         
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </section>
                      <!--End About Section Two -->


                       <!-- Offers Section -->
                      <section>
                          <div class="auto-container">
                              <div class="sec-title text-center">
                                  <span class="title">Our Facilities</span>
                                  <h2>What We Offer</h2>
                              </div>

                              <div class="row justify-content-center">
                                  <!-- Offer Block -->
                                  <div class="offer-block col-xl-2 col-lg-3 col-md-6 col-sm-12">
                                      <div class="inner-box">
                                          <div class="icon-box wow rollIn"><span class="icon flaticon-abc-block"></span></div>
                                          <h5><a href="about.html">Full Day Session</a></h5>
                                      </div>
                                  </div>

                                  <!-- Offer Block -->
                                  <div class="offer-block col-xl-2 col-lg-3 col-md-6 col-sm-12">
                                      <div class="inner-box">
                                          <div class="icon-box wow rollIn" data-wow-delay="300ms"><span class="icon flaticon-parents"></span></div>
                                          <h5><a href="about.html">Qualified Teachers</a></h5>
                                      </div>
                                  </div>

                                  <!-- Offer Block -->
                                  <div class="offer-block col-xl-2 col-lg-3 col-md-6 col-sm-12">
                                      <div class="inner-box">
                                          <div class="icon-box wow rollIn" data-wow-delay="600ms"><span class="icon flaticon-teddy-bear-1"></span></div>
                                          <h5><a href="about.html">Play <br>Areas</a></h5>
                                      </div>
                                  </div>

                                  <!-- Offer Block -->
                                  <div class="offer-block col-xl-2 col-lg-3 col-md-6 col-sm-12">
                                      <div class="inner-box">
                                          <div class="icon-box wow rollIn" data-wow-delay="0ms"><span class="icon flaticon-horse"></span></div>
                                          <h5><a href="about.html">Activity Room</a></h5>
                                      </div>
                                  </div>

                                  <!-- Offer Block -->
                                  <div class="offer-block col-xl-2 col-lg-3 col-md-6 col-sm-12">
                                      <div class="inner-box">
                                          <div class="icon-box wow rollIn" data-wow-delay="300ms"><span class="icon flaticon-knowledge"></span></div>
                                          <h5><a href="about.html">Knowledge Classes</a></h5>
                                      </div>
                                  </div>

                                  <!-- Offer Block -->
                                  <div class="offer-block col-xl-2 col-lg-3 col-md-6 col-sm-12">
                                      <div class="inner-box">
                                          <div class="icon-box wow rollIn" data-wow-delay="600ms"><span class="icon flaticon-stopwatch"></span></div>
                                          <h5><a href="about.html">Last Minute Request</a></h5>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </section>
                      <!--End Offer Section -->


                       <!-- Fluid Section One -->
                      <section class="fluid-section-one">
                          <div class="outer-container clearfix">
                              
                              
                              <!--Content Column-->
                              <div class="content-column">
                                  <div class="parallax-scene parallax-scene-2 anim-icons">
                                      <span data-depth="0.50" class="parallax-layer icon icon-cloud-2"></span>
                                      <span data-depth="0.30" class="parallax-layer icon icon-cloud-3"></span>
                                      <span data-depth="0.20" class="parallax-layer icon icon-butterfly"></span>
                                      <span data-depth="0.40" class="parallax-layer icon icon-star-6"></span>
                                  </div>
                                  <div class="inner-column">
                                      <!-- Sec Title -->
                                      <div class="sec-title light">
                                          <div class="title">Things For Kids</div>
                                          <h2>Kids Activites</h2>
                                      </div>
                                      <div class="text">In this direction, the school has instituted the following important activities:</div>
                                      <ul class="list-style-one">
                                          <li>Sports and Physical Fitness</li>
                                          <li>Regular Health  Checkups</li>
                                          <li>Days of National Importance</li>
                                          <li>Celebration of important festivals</li>
                                          <li>Celebration of birthdays of prominent leaders and personalities</li>
                                          <li>Cultural function</li>
                                          <li>Essay writing, Poetry and Elocution Competitions</li>
                                          <li>Teachers’ Day Celebration Educational visits</li>
                                          <li>Educational visits</li>
                                      </ul>
                                  </div>
                              </div>
                              <!--Image Column-->
                              <div class="image-column">
                                  <div class="inner-column" style="background-image:url(img/h6.jpg);">
                                      <figure class="image-box"><img src="{{ asset ('img/h6.jpg') }}" alt=""></figure>
                                  </div>
                                 
                              </div>
                          </div>
                      </section>
                      <!-- End Fluid Section One -->

                  @endsection