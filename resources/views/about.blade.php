@extends('layouts.appfront')

@section('css')

<style type="text/css">
    table { 
    width: 750px; 
    border-collapse: collapse; 
    margin:50px auto;
    }

/* Zebra striping */
tr:nth-of-type(odd) { 
    background: #eee; 
    }

th { 
    background: #3498db; 
    color: white; 
    font-weight: bold; 
    }

td, th { 
    padding: 10px; 
    border: 1px solid #ccc; 
    text-align: left; 
    font-size: 18px;
    }

/* 
Max width before this PARTICULAR table gets nasty
This query will take effect for any screen smaller than 760px
and also iPads specifically.
*/
@media 
only screen and (max-width: 760px),
(min-device-width: 768px) and (max-device-width: 1024px)  {

    table { 
        width: 100%; 
    }

    /* Force table to not be like tables anymore */
    table, thead, tbody, th, td, tr { 
        display: block; 
    }
    
    /* Hide table headers (but not display: none;, for accessibility) */
    thead tr { 
        position: absolute;
        top: -9999px;
        left: -9999px;
    }
    
    tr { border: 1px solid #ccc; }
    
    td { 
        /* Behave  like a "row" */
        border: none;
        border-bottom: 1px solid #eee; 
        position: relative;
        padding-left: 50%; 
    }

    td:before { 
        /* Now like a table header */
        position: absolute;
        /* Top/left values mimic padding */
        top: 6px;
        left: 6px;
        width: 45%; 
        padding-right: 10px; 
        white-space: nowrap;
        /* Label the data */
        content: attr(data-column);

        color: #000;
        font-weight: bold;
    }

}
</style>
@endsection

@section('content')

  <!--Page Title-->
    <section class="page-title" style="background-image:url(img/banner.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
              
                <h1>About Us</h1>
            </div>
        </div>
    </section>
    <!--End Page Title-->



        <!-- About Section Two -->
    <section class="about-section">
        <div class="auto-container">
            <div class="parallax-scene parallax-scene-1 anim-icons">
                <span data-depth="0.20" class="parallax-layer icon icon-rainbow-3"></span>
                <span data-depth="0.40" class="parallax-layer icon icon-star-2"></span>
                <span data-depth="0.30" class="parallax-layer icon icon-star-5"></span>
                <span data-depth="0.50" class="parallax-layer icon icon-plane-2"></span>
            </div>

            <div class="sec-title text-center">
                <span class="title">Vidya Prachara Sangha (R)</span>
                <h2>About School</h2>
            </div>

            <div class="row">
                <!-- Content Column -->
                <div class="content-column col-lg-6 col-md-12 col-sm-12 order-2">
                    <div class="inner-column">
                    	<h5>Value Based Education and Activities at Vijaya Kids Care :</h5><br>
                        <div class="text" align="justify">Vidya Prachara Sangha Reg started in the year 1946 before 
independence. The aims and objective of the school is to provide quality education to 
the rural children. It was the brain child of the 15 philanthropist who have 
successfully contributed to the establishment of the institution. To create 
facility for Higher primary shcool, Highschool, Juniorcollege, First Grade College 
and College of Education. Accordingly all the units are running at present with 
the capacity of 1500 students and having a reputation report of among all the 
stakeholders including the university. Vijaya Higher Primary School is a student 
centred organisation delivering excellence in education. We believe that our 
cohesion and morale help us to achieve excellence in our school. </div>
                  
                      
                    </div>
                </div>

                   <div class="image-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-column">
                        <figure class="image wow fadeInRight"><img src="{{ asset ('img/h3.jpg') }}" alt=""></figure>
                    
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End About Section Two -->

     <!-- Testimonial Section -->
    <section class="testimonial-section">
        <div class="auto-container">
            <div class="sec-title text-center">
   
                <h2>Vision & Mission</h2>
            </div>

           <div class="row">
                <!-- Testimonial Block -->
                <div class="testimonial-block col-6">
                    <div class="inner-box">
                        <div class="content">
                            <div class="text" align="justify">To provide a safe haven where everyone is valued and respected. All staff members, in partnership with parents and families are fully committed to students education and career readiness. Students are empowered to meet current and future challenges to develop social awareness, civic responsibility and personal Growth. </div>
                        </div>
                        <div class="info-box">
                            <div class="thumb"><img src="{{ asset ('img/vision.png') }}" alt=""></div>
                            <h6 class="name">MSSION</h6>
                         
                        </div>
                    </div>
                </div>

                <!-- Testimonial Block -->
                <div class="testimonial-block style-two col-6">
                    <div class="inner-box">
                        <div class="content">
                            <div class="text" align="justify">To prepare and motivate our students for a rapidly changing world by instilling in them critical thinking skills, a global perspective and a respect for core values of honesty, loyalty, perseverance and compassion. Students will have success for today and be prepared for tomorrow. </div>
                        </div>
                        <div class="info-box">
                            <div class="thumb"><img src="{{ asset ('img/mission.png') }}" alt=""></div>
                            <h6 class="name">VISION</h6>
                         
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>
    <!--End Testimonial Section -->


<!--  <section class="testimonial-section">
        <div class="auto-container">
            <div class="row">
                    <table>
                      <thead>
                        <tr>
                          <th>First Name</th>
                          <th>Last Name</th>
                          <th>Job Title</th>
                          <th>Twitter</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td data-column="First Name">James</td>
                          <td data-column="Last Name">Matman</td>
                          <td data-column="Job Title">Chief Sandwich Eater</td>
                          <td data-column="Twitter">@james</td>
                        </tr>
                        <tr>
                          <td data-column="First Name">Andor</td>
                          <td data-column="Last Name">Nagy</td>
                          <td data-column="Job Title">Designer</td>
                          <td data-column="Twitter">@andornagy</td>
                        </tr>
                        <tr>
                          <td data-column="First Name">Tamas</td>
                          <td data-column="Last Name">Biro</td>
                          <td data-column="Job Title">Game Tester</td>
                          <td data-column="Twitter">@tamas</td>
                        </tr>
                        <tr>
                          <td data-column="First Name">Zoli</td>
                          <td data-column="Last Name">Mastah</td>
                          <td data-column="Job Title">Developer</td>
                          <td data-column="Twitter">@zoli</td>
                        </tr>
                        <tr>
                          <td data-column="First Name">Szabi</td>
                          <td data-column="Last Name">Nagy</td>
                          <td data-column="Job Title">Chief Sandwich Eater</td>
                          <td data-column="Twitter">@szabi</td>
                        </tr>
                      </tbody>
                    </table>
                </div>
            </div>
        </section> -->

         <section class="team-section" id="toppers">
            <div class="auto-container">
                <div class="sec-title text-center">
                   
                    <h2>Self Affidavit of School</h2>
                </div>

                <div class="row">
                    <!-- Team Block -->
                    <div class="team-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp">
                      
                        <img src="{{ asset ('img/aff1.jpg') }}" alt="">
                               
                    
                    </div>

                       <div class="team-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp">
                      
                        <img src="{{ asset ('img/aff2.jpg') }}" alt="">
                               
                    
                    </div>

                </div>
            </div>
        </section>

@endsection