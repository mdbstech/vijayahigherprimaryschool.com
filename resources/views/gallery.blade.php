@extends('layouts.appfront')

@section('css')



<style type="text/css">
	.portfolio-menu {
    text-align: center;
    margin: 30px auto;
}

.portfolio-menu ul li {
    display: inline-block;
    margin: 0;
    list-style: none;
    padding: 10px 15px;
    border: 1px solid #ff0000;
    cursor: pointer;
    transition: all .5 ease;
}

.portfolio-menu ul {
    padding:0;
}

.portfolio-menu ul li:hover {
    background: #ff0000;
    color: #fff;
}

.portfolio-menu ul li.active {
    background:  #ff0000;
    color: #fff;
}

.portfolio-item {
    max-width: 1000px;
    margin: 30px auto;
}

.portfolio-item .item {
    width: 33.3%;
    display: inline-block;
    margin-bottom: 10px;
    padding: 0 10px;
}

.portfolio-item .item div {
    height: 280px;
    background-position: center;
    background-size: cover;
    background-repeat: no-repeat;
}

@media screen and (max-width: 1024px) {
  .portfolio-item .item {
    width: 50%;
    padding:0 15px;
}
}

@media screen and (max-width: 640px) {
  .portfolio-item .item {
    width: 100%;
}
}


</style>

@endsection

@section('content')

  <!--Page Title-->
    <section class="page-title" style="background-image:url(img/banner.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
              
                <h1>Gallery</h1>
            </div>
        </div>
    </section>
    <!--End Page Title-->


<div class="container text-center">

  <div class="portfolio-menu">
      <ul>
      	@if($albums)
        <li class="active" data-filter=".all">All</li>
        @foreach($albums as $album)
        <li data-filter=".{{$album->album_id}}">{{$album->title}}</li>
      	@endforeach
        @endif
      
      </ul>
  </div>
</div>


<div class="portfolio-item">

	@if($photos)
        @foreach($photos as $photo)
			<div class="item all">
			      <div><img src="{{ asset('storage/photos/'.$photo->image) }}" alt="{{$photo->photo_id}}"></div>
			</div>
		@endforeach
		@foreach($photos as $photo)
			<div class="item {{$photo->album_id}}">
			      <div><img src="{{ asset('storage/photos/'.$photo->image) }}" alt="{{$photo->photo_id}}"></div>
			</div>
   		@endforeach
    @endif
  
</div>


@endsection

@section('js')

<script>
	$(document).ready(function(){
  $('.portfolio-item').isotope(function(){
      itemSelector:'.item'
    });



  $('.portfolio-menu ul li').click(function(){
    $('.portfolio-menu ul li').removeClass('active');
    $(this).addClass('active');


    var selector = $(this).attr('data-filter');
      $('.portfolio-item').isotope({
        filter: selector
      })
      return false;
  });
});</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js"></script>

@endsection