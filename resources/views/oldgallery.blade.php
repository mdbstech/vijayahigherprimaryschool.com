@extends('layouts.appfront')

@section('content')

 <!--Page Title-->
    <section class="page-title" style="background-image:url(img/banner.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
              
                <h1>Gallery</h1>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Gallery Section -->
    <section class="gallery-section style-two">   
        <div class="auto-container">
            <div class="row">

                
                                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g21.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g21.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>

                                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g22.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g22.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>

                                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g23.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g23.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>

                                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g24.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g24.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>

                                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g25.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g25.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>

                                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g26.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g26.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>
                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g1.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g1.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>
                </div>

                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="400ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g2.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g2.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>
                </div>

                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g3.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g3.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>
                </div>

                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g4.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g4.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>
                </div>

                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="400ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g5.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g5.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>
                </div>

                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g6.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g6.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>
                </div>

                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g7.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g7.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>
                </div>

                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="400ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g8.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g8.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>
                </div>

                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g9.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g9.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>


                </div>

                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g10.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g10.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>


                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g11.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g11.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>


                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g12.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g12.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>


                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g13.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g13.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>


                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g14.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g14.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>


                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g15.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g15.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>


                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g16.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g16.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>


                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g17.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g17.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>


                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g18.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g18.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>


                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g19.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g19.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>


                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/g20.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/g20.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>

            </div>
        </div>
    </section>
    <!--End Gallery Section -->

@endsection