@extends('layouts.appfront')

@section('content')

   <!--Page Title-->
    <section class="page-title" style="background-image:url(img/banner.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
              
                <h1>Academics</h1>
            </div>
        </div>
    </section>
    <!--End Page Title-->



   <!-- Fluid Section One -->
                      <section class="fluid-section-one" id="curriculum">
                          <div class="outer-container clearfix">
                              
                              
                              <!--Content Column-->
                              <div class="content-column">
                                  <div class="parallax-scene parallax-scene-2 anim-icons">
                                      <span data-depth="0.50" class="parallax-layer icon icon-cloud-2"></span>
                                      <span data-depth="0.30" class="parallax-layer icon icon-cloud-3"></span>
                                      <span data-depth="0.20" class="parallax-layer icon icon-butterfly"></span>
                                      <span data-depth="0.40" class="parallax-layer icon icon-star-6"></span>
                                  </div>
                                  <div class="inner-column">
                                      <!-- Sec Title -->
                                      <div class="sec-title light">
                                          <div class="title">Things For Kids</div>
                                          <h2>CURRICULUM</h2>
                                      </div>
                                      <div class="text" align="justify">Requires students to identify a research problem, develop a design for the study and write a research proposal. Provides opportunities to evaluate and interpret research literature.<br>

                                      curriculum will systemically address technology integration, evidenced-based practices, innovative and collaborative learning experiences, and the impact of social, political, psychological, and economic factors.<br>

                                      Evaluates historical and current trends in educational curriculum development and their impact on public and non-public schools from an instructional leadership perspective.<br>

                                       It builds mathematical knowledge and with a focus on supporting high quality mathematics education.<br>


                                      Curriculum should be reviewed and revised on a regular basis so that it is able to serve the changing needs of both students and society.</div>






                                      <ul class="list-style-one">
                                          	<li>Conceptualizing the curriculum</li>
											<li>Selecting and organizing the content, material and learning experiences</li>
											<li>Suggesting the method and ways of providing these experiences</li>
											<li>Evaluating the learning outcomes in terms of attainment of desired educational objectives</li>





                                      </ul>
                                  </div>
                              </div>
                              <!--Image Column-->
                              <div class="image-column">
                                  <div class="inner-column" style="background-image:url(img/h7.jpg);">
                                      <figure class="image-box"><img src="{{ asset ('img/h7.jpg') }}" alt=""></figure>
                                  </div>
                                  <div class="caption-box"><h3>Education for Life</h3></div>
                              </div>
                          </div>
                      </section>
                      <!-- End Fluid Section One -->



            <!-- About Section Two -->
    <section class="about-section">
        <div class="auto-container">
            <div class="parallax-scene parallax-scene-1 anim-icons">
                <span data-depth="0.20" class="parallax-layer icon icon-rainbow-3"></span>
                <span data-depth="0.40" class="parallax-layer icon icon-star-2"></span>
                <span data-depth="0.30" class="parallax-layer icon icon-star-5"></span>
                <span data-depth="0.50" class="parallax-layer icon icon-plane-2"></span>
            </div>

        

            <div class="row">
                <!-- Content Column -->
                <div class="content-column col-lg-6 col-md-12 col-sm-12 order-2">
                    <div class="inner-column">
                       
                       <ul class="list-style-one"> 
                      <li style="color: black;"> The concept of State syllabus curriculum teaching as teaching of skills and not only the content needs be drilled into the teachers.</li>
                     <li style="color: black;"> The textbooks need to incorporate activities and questions which give space, time and freedom for inculcating creativity and developing imagination of the child. </li>
                    <li style="color: black;"> Emphasis should be more on the listening and speaking skills while designing books. </li>
                     <li style="color: black;"> All teachers teaching English need to be trained in the use of phonetics as clarity and intelligibility are the two major dimensions of proper pronunciation.</li>
                     <li style="color: black;"> Training programs must have a component of peer teaching and demo teaching along with its     in-depth and objective analysis and realistic evaluation. </li>
                    <li style="color: black;"> Under CRCs/ BRCs, local teacher development groups should be set up for teachers to exchange notes and resolve any problem and apprehension regarding teaching of English.</li>

                  </ul>
                      
                    </div>
                </div>

                   <div class="image-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-column">
                        <figure class="image wow fadeInRight"><img src="{{ asset ('img/curriculum.jpg') }}" alt=""></figure>
                    
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End About Section Two -->



    <!-- Program Section -->
    <section class="program-section alternate" id="facilities">
        <div class="auto-container">

                <div class="sec-title text-center">
           
                <h2>Infrastructure</h2>
            </div>
            <div class="row">

                <!-- Program Block -->
                <div class="program-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/sports.jpg') }}" alt=""></figure>
                        </div>
                        <div class="lower-content">
                            <h4><a href="numbers-matching.html">Playground</a></h4>
                           
                        
                        </div>
                    </div>
                </div>


                 <div class="program-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/basketball.jpg') }}" alt=""></figure>
                        </div>
                        <div class="lower-content">
                            <h4><a href="numbers-matching.html">Basketball</a></h4>
                           
                        
                        </div>
                    </div>
                </div>


                 <div class="program-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/auditorium.jpg') }}" alt=""></figure>
                        </div>
                        <div class="lower-content">
                            <h4><a href="numbers-matching.html">Auditorium</a></h4>
                           
                        
                        </div>
                    </div>
                </div>

                 <div class="program-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/transportation.jpg') }}" alt=""></figure>
                        </div>
                        <div class="lower-content">
                            <h4><a href="numbers-matching.html">Transportation</a></h4>
                           
                        
                        </div>
                    </div>
                </div>

                  <div class="program-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/chemistrylab.jpg') }}" alt=""></figure>
                        </div>
                        <div class="lower-content">
                            <h4><a href="numbers-matching.html">Chemistry Lab</a></h4>
                           
                        
                        </div>
                    </div>
                </div>


                  <div class="program-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/computerlab.jpg') }}" alt=""></figure>
                        </div>
                        <div class="lower-content">
                            <h4><a href="numbers-matching.html">Computer Lab</a></h4>
                           
                        
                        </div>
                    </div>
                </div>

                  <div class="program-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/classroom.jpg') }}" alt=""></figure>
                        </div>
                        <div class="lower-content">
                            <h4><a href="numbers-matching.html">Class Rooms</a></h4>
                           
                        
                        </div>
                    </div>
                </div>



                  <div class="program-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/sportsroom.jpg') }}" alt=""></figure>
                        </div>
                        <div class="lower-content">
                            <h4><a href="numbers-matching.html">Sports Room</a></h4>
                           
                        
                        </div>
                    </div>
                </div>

                     <div class="program-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/arts.jpg') }}" alt=""></figure>
                        </div>
                        <div class="lower-content">
                            <h4><a href="numbers-matching.html">Arts & Crafts Room</a></h4>
                           
                        
                        </div>
                    </div>
                </div>


                       <div class="program-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/h12.jpg') }}" alt=""></figure>
                        </div>
                        <div class="lower-content">
                            <h4><a href="numbers-matching.html">Fire Extinguisher</a></h4>
                           
                        
                        </div>
                    </div>
                </div>

                       <div class="program-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/h14.jpg') }}" alt=""></figure>
                        </div>
                        <div class="lower-content">
                            <h4><a href="numbers-matching.html">Water Tank</a></h4>
                           
                        
                        </div>
                    </div>
                </div>

                       <div class="program-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/h13.jpg') }}" alt=""></figure>
                        </div>
                        <div class="lower-content">
                            <h4><a href="numbers-matching.html">Activity Room</a></h4>
                           
                        
                        </div>
                    </div>
                </div>

                        <div class="program-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/health.jpg') }}" alt=""></figure>
                        </div>
                        <div class="lower-content">
                            <h4><a href="numbers-matching.html">First Aid</a></h4>
                           
                        
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </section>



   <!-- Fluid Section One -->
                      <section class="fluid-section-one" id="library">
                          <div class="outer-container clearfix">
                              
                              
                              <!--Content Column-->
                              <div class="content-column">
                                  <div class="parallax-scene parallax-scene-2 anim-icons">
                                      <span data-depth="0.50" class="parallax-layer icon icon-cloud-2"></span>
                                      <span data-depth="0.30" class="parallax-layer icon icon-cloud-3"></span>
                                      <span data-depth="0.20" class="parallax-layer icon icon-butterfly"></span>
                                      <span data-depth="0.40" class="parallax-layer icon icon-star-6"></span>
                                  </div>
                                  <div class="inner-column">
                                      <!-- Sec Title -->
                                      <div class="sec-title light">
                                          <div class="title">Things For Kids</div>
                                          <h2>LIBRARY</h2>
                                      </div>
                                      <div class="text" align="justify">•	A school without a library is unthinkable these days. It must have books on various subjects, newspapers, journals and magazines. Students, those especially who want to add to their knowledge, go to the library in their vacant periods and study. It adds to their knowledge.<br>

                                      •	A school without a library is unthinkable these days. It must have books on various subjects, newspapers, journals and magazines. Students, those especially who want to add to their knowledge, go to the library in their vacant periods and study. It adds to their knowledge.<br>

                                      •	A school without a library is unthinkable these days. It must have books on various subjects, newspapers, journals and magazines. Students, those especially who want to add to their knowledge, go to the library in their vacant periods and study. It adds to their knowledge.<br>

                                       •	On one side of the library hall there are long tables and benches. Students sit there and read newspapers, journals and magazines; some take down notes. No body is permitted to talk to disturb others.<br>


                                      •	Every class has one library period once a week. Students go to the library in the period and study.<br>

                                  		•	The library also has reference and text books. These are meant for studying in the library only. The calm and quiet atmosphere of the library helps the students to work attentively.<br>

										•	The school librarian is a trained and qualified person and well-experienced. He gives advice to students on the choice of books. He is an expert in his work. He understands his duty well and is of great help to students. We find him busy all the time. He advises students not to spoil books by scribbling in them or tearing off the pages.
										</div>

                                  </div>
                              </div>
                              <!--Image Column-->
                              <div class="image-column">
                                  <div class="inner-column" style="background-image:url(img/library.jpg);">
                                      <figure class="image-box"><img src="{{ asset ('img/library.jpg') }}" alt=""></figure>
                                  </div>
                                  <div class="caption-box"><h3>Education for Life</h3></div>
                              </div>
                          </div>
                      </section>
                      <!-- End Fluid Section One -->



                      <!-- Event Section -->
    <section class="events-section style-two" id="calendar">
        <div class="auto-container">
            <div class="row">

                <div class="sec-title">
                                        
                   <h2>ACADEMIC CALENDER OF THE SCHOOL</h2>
                </div>
                <!-- Event Block -->
                <div class="event-block col-xl-4 col-md-12 col-sm-12 wow fadeInLeft">
                                        
                        <div class="content-box">
                            <div class="date">FIRST TEST-2019</div>
                            <h6>Revision –JULY 24th to JULY 29th </h6>
                            <h6> Test-29/072019 to 05/08/2019</h6>
                        </div>
                    
                </div>

                  <div class="event-block col-xl-4 col-md-12 col-sm-12 wow fadeInLeft">
                                        
                        <div class="content-box">
                           <div class="date"> SECOND TEST-2019</div>
                            <h6> Revision-AUGUST 24TH TO 29TH</h6>
                          <h6> Test-30/08/2019 to 07/09/2019</h6>                     
                        </div>
                    
                </div>


                <div class="event-block col-xl-4 col-md-12 col-sm-12 wow fadeInLeft">
                                        
                        <div class="content-box">
                           <div class="date">MID TERM EXAM-2019</div>
                            <h6>Revision</h6>
                            <h6>Test-23/09/2019 TO 04/10/2019</h6>                        
                        </div>
                    
                </div>


                <div class="event-block col-xl-4 col-md-12 col-sm-12 wow fadeInLeft">
                                        
                        <div class="content-box">
                           <div class="date">THIRD TEST-2019</div>
                          <h6>Revision-JAN 12TH TO JAN 22ND </h6>
                          <h6>Test-23/01/2020 TO 04/01/2020</h6>
                       
                        </div>
                    
                </div>


                <div class="event-block col-xl-4 col-md-612 col-sm-12 wow fadeInLeft">
                                        
                        <div class="content-box">
                          <div class="date">FORTH TEST-2019</div>
                          <h6>Revision- FEB 22 TH TO FEB 28TH</h6>
                          <h6>Test-01/03/2019 to 06/03/2020 </h6> 
                       
                        </div>
                    
                </div>


                <div class="event-block col-xl-4 col-md-12 col-sm-12 wow fadeInLeft">
                                        
                        <div class="content-box">
                          <div class="date"> ANNUAL EXAM</div>
                            <h6>TesT-14/03/2020 TO 05/04/2020</h6>
                      
                        </div>
                    
                </div>

              </div>
            </div>
    </section>


    <!-- Team Section -->
    <section class="team-section" id="toppers">
        <div class="auto-container">
            <div class="sec-title text-center">
                <div class="title">Meet Our Toppers</div>
                <h2>Academic Toppers</h2>
            </div>

            <div class="row">
                <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/prekg.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#"> GAHAN.V.YADAV  </a></h6>
                            <span class="designation">Pre kg</span>
                        </div>
                    </div>
                </div>

                <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="400ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/prekg1.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">JANAVI </a></h6>
                            <span class="designation">Pre kg</span>
                        </div>
                    </div>
                </div>

                <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="800ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/lkga.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">MOULYA G L</a></h6>
                            <span class="designation">LKG A</span>
                        </div>
                    </div>
                </div>

                <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/lkga2.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">YASH RAJ C R</a></h6>
                            <span class="designation">LKG A</span>
                        </div>
                    </div>
                </div>



                <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/lkgb.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">DHANVITH KUMAR </a></h6>
                            <span class="designation">LKG B</span>
                        </div>
                    </div>
                </div>

                     <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/lkgb1.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">MANVITHA A S</a></h6>
                            <span class="designation">LKG B</span>
                        </div>
                    </div>
                </div>

                            <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/ukg.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">MONISHA B M</a></h6>
                            <span class="designation">UKG</span>
                        </div>
                    </div>
                </div>

                                  <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/ukg2.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">KHUSHAL. R</a></h6>
                            <span class="designation">UKG</span>
                        </div>
                    </div>
                </div>

                                  <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/ukga.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">MAITHREKA. E</a></h6>
                            <span class="designation">UKG A</span>
                        </div>
                    </div>
                </div>

                                  <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/ukgb.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">MOHANGOWDA G</a></h6>
                            <span class="designation">UKG B</span>
                        </div>
                    </div>
                </div>


                                    <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/1st.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">VIHANGOWDA. C</a></h6>
                            <span class="designation">1 Standard</span>
                        </div>
                    </div>
                </div>

                                      <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/1st1.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">HEMANTH. H.N</a></h6>
                            <span class="designation">1 Standard</span>
                        </div>
                    </div>
                </div>

                                      <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/1st2.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">LEKHANA.D</a></h6>
                            <span class="designation">1 Standard</span>
                        </div>
                    </div>
                </div>

                                      <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/1stb.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">GANAVI.R.N</a></h6>
                            <span class="designation">1 Standard B</span>
                        </div>
                    </div>
                </div>


                                            <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/2nd.jpeg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">BIMBITHA.B.K</a></h6>
                            <span class="designation">2 Standard </span>
                        </div>
                    </div>
                </div>

                                             <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/2nd1.jpeg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">CHINTHAN. K.H</a></h6>
                            <span class="designation">2 Standard</span>
                        </div>
                    </div>
                </div>

                                             <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/2ndb.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">VARNIKA. C</a></h6>
                            <span class="designation">2 Standard B</span>
                        </div>
                    </div>
                </div>

                                             <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/2ndb1.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">LASHITH GOWDA. G</a></h6>
                            <span class="designation">2 Standard B</span>
                        </div>
                    </div>
                </div>




                                                      <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/3rd.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">AKIFA SAHER </a></h6>
                            <span class="designation">3 Standard</span>
                        </div>
                    </div>
                </div>

                                                            <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/3rd1.jpeg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">HARSHUL.S </a></h6>
                            <span class="designation">3 Standard</span>
                        </div>
                    </div>
                </div>

                                                            <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/3rd2.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">GURUCHARAN.V.M</a></h6>
                            <span class="designation">3 Standard</span>
                        </div>
                    </div>
                </div>


                                                               <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/4th.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">CHAITHANYA.B.T</a></h6>
                            <span class="designation">4 Standard</span>
                        </div>
                    </div>
                </div>

                                                                   <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/4th1.jpeg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">CHETHANGOWDA.J.M</a></h6>
                            <span class="designation">4 Standard</span>
                        </div>
                    </div>
                </div>


                                                                   <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/5th1.jpeg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">AFEEFA SAHER</a></h6>
                            <span class="designation">5 Standard</span>
                        </div>
                    </div>
                </div>


                                                                   <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/5th2.jpeg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">ARJUN LOKESH</a></h6>
                            <span class="designation">5 Standard</span>
                        </div>
                    </div>
                </div>

                                                                             <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/6th.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">POORVIKA.P.P</a></h6>
                            <span class="designation">6 Standard</span>
                        </div>
                    </div>
                </div>

                                                                                     <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/7th.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">PAVAN.M S</a></h6>
                            <span class="designation">6 Standard</span>
                        </div>
                    </div>
                </div>


                                                                                           <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/7th1.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">BINDU.V</a></h6>
                            <span class="designation">7 Standard</span>
                        </div>
                    </div>
                </div>

                                                                                           <!-- Team Block -->
                <div class="team-block col-lg-2 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ asset ('img/toppers/7th2.jpg') }}" style="height: 200px;" alt=""></figure>
                           
                        </div>
                        <div class="caption-box">
                            <h6 class="name"><a href="#">DISHA.V</a></h6>
                            <span class="designation">7 Standard</span>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>
    <!--End Team Section -->



    <!-- Program Section -->
    <section class="program-section alternate" id="facilities">
        <div class="auto-container">

                <div class="sec-title text-center">
           
                <h2>Transfer Certificate</h2>
            </div>
            <div class="row" align="center">

                <!-- Program Block -->
                <div class="program-block col-lg-12 col-md-12 col-sm-12 wow fadeInUp">
                   
                           <img src="{{ asset ('img/tc.jpg') }}" alt="">
                    
                </div>

            </div>
        </div>
    </section>

@endsection