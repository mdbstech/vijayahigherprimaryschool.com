@extends('layouts.appfront')

@section('content')

   <!--Page Title-->
    <section class="page-title" style="background-image:url(img/banner.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
              
                <h1>Contact Us</h1>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Contact Map Section -->
    <section class="contact-map-section">
        <!--Map Outer-->
        <div class="map-outer">
          
        	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3784.3326108505194!2d76.66910046736292!3d12.501439789523975!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x2ae4f01ff2a6cdd0!2sVijaya%20College!5e0!3m2!1sen!2sin!4v1584100872392!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

        </div>
        <div class="row no-gutters">
            <div class="info-box col-md-4 col-sm-12">
                <h5 style="color: white;">vpssecretary@gmail.com <br> vkclhpschool@gmail.com</h5>
            </div>
            <div class="info-box col-md-4 col-sm-12">
                <h5 style="color: white;">08236256485 <br> 9945846111 / 7090447609</h5>
            </div>
            <div class="info-box col-md-4 col-sm-12">
                <h5 style="color: white;">Vidya Prachara Sangha ®, Vijaya Higher Primary School, 2nd Cross, Krishnanagara, Pandavapura Town & Tq, Mandya Dist-571434</h5> 
            </div>
        </div>
    </section>
    <!-- End Map Section -->


@endsection