@extends('layouts.appfront')

@section('css')

<style type="text/css">
    table { 
    width: 750px; 
    border-collapse: collapse; 
    margin:50px auto;
    }

/* Zebra striping */
tr:nth-of-type(odd) { 
    background: #eee; 
    }

th { 
    background: #3498db; 
    color: white; 
    font-weight: bold; 
    }

td, th { 
    padding: 10px; 
    border: 1px solid #ccc; 
    text-align: left; 
    font-size: 18px;
    }

/* 
Max width before this PARTICULAR table gets nasty
This query will take effect for any screen smaller than 760px
and also iPads specifically.
*/
@media 
only screen and (max-width: 760px),
(min-device-width: 768px) and (max-device-width: 1024px)  {

    table { 
        width: 100%; 
    }

    /* Force table to not be like tables anymore */
    table, thead, tbody, th, td, tr { 
        display: block; 
    }
    
    /* Hide table headers (but not display: none;, for accessibility) */
    thead tr { 
        position: absolute;
        top: -9999px;
        left: -9999px;
    }
    
    tr { border: 1px solid #ccc; }
    
    td { 
        /* Behave  like a "row" */
        border: none;
        border-bottom: 1px solid #eee; 
        position: relative;
        padding-left: 50%; 
    }

    td:before { 
        /* Now like a table header */
        position: absolute;
        /* Top/left values mimic padding */
        top: 6px;
        left: 6px;
        width: 45%; 
        padding-right: 10px; 
        white-space: nowrap;
        /* Label the data */
        content: attr(data-column);

        color: black;
        font-weight: bold;
    }

}
</style>
@endsection

@section('content')

  <!--Page Title-->
    <section class="page-title" style="background-image:url(img/banner.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
              
                <h1>Norms For Fixing Fees</h1>
            </div>
        </div>
    </section>
    <!--End Page Title-->


 <section class="testimonial-section">
        <div class="auto-container">
           <h2 align="center">2019-20</h2>
            <div class="row">

             
                    <table>
                 
                      <tbody>
                        <tr>
                          <td> 2019-20 Strength </td>
                          <td>331 Students</td>
                                                         
                        </tr>
                        <tr>
                          <td>Estimated New Admission for 2020-21</td>
                         <td>34 Students</td>

                        </tr>
                        <tr>
                          <td><b>Total No.of Students for 2020-21</b></td>
                          <td><b>350 Students</b></td>
                     
                        </tr>
                     

                      </tbody>
                    </table>
                </div>
            </div>
        </section>



 <section class="testimonial-section">
        <div class="auto-container">
              <h2 align="center">2020-21</h2>
            <div class="row">
          
                    <table>
                  
                      <tbody>
                        <tr>
                          <td>2020-21  Total Strength  </td>
                          <td>350 Students</td>
                                                         
                        </tr>
                        <tr>
                          <td>Teacher Ratio</td>
                         <td>30:1</td>

                        </tr>
                        <tr>
                          <td>Accordingly, 200/30=7 approximately 7 Teachers required for 2020-21, for the class 1st to 8th Standard.</td>
                          <td>350 Students</td>
                     
                        </tr>

                        <tr>
                          <td>Present Teachers</td>
                          <td>10</td>
                     
                        </tr>

                         <tr>
                          <td>Part time teachers for Craft & PET</td>
                          <td>2</td>
                     
                        </tr>
                     

                      </tbody>
                    </table>
                </div>
            </div>
        </section>
  

@endsection