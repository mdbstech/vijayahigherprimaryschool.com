@extends('layouts.appfront')

@section('content')

 <!--Page Title-->
    <section class="page-title" style="background-image:url(img/banner.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
              
                <h1>Gallery</h1>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Gallery Section -->
    <section class="gallery-section style-two">   
        <div class="auto-container">
            <div class="row">

      
                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/newyear2021/ny1.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/newyear2021/ny1.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>
                </div>

                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="400ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/newyear2021/ny2.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/newyear2021/ny2.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>
                </div>

                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/newyear2021/ny3.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/newyear2021/ny3.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>
                </div>

                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/newyear2021/ny4.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/newyear2021/ny4.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>
                </div>

                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="400ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/newyear2021/ny5.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/newyear2021/ny5.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>
                </div>

                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/newyear2021/ny6.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/newyear2021/ny6.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>
                </div>

                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/newyear2021/ny7.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/newyear2021/ny7.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>
                </div>

                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="400ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/newyear2021/ny8.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/newyear2021/ny8.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>
                </div>

                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/newyear2021/ny9.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/newyear2021/ny9.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>


                </div>

                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/newyear2021/ny10.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/newyear2021/ny10.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>


                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/newyear2021/ny11.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/newyear2021/ny11.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>


                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/newyear2021/ny12.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/newyear2021/ny12.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>


                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/newyear2021/ny13.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/newyear2021/ny13.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>


                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/newyear2021/ny14.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/newyear2021/ny14.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>


                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/newyear2021/ny15.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/newyear2021/ny15.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>


                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/newyear2021/ny16.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/newyear2021/ny16.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>


                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/newyear2021/ny17.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/newyear2021/ny17.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>


                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/newyear2021/ny18.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/newyear2021/ny18.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>



            </div>
        </div>
    </section>
    <!--End Gallery Section -->

@endsection