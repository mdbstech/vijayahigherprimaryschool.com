@extends('admin.layouts.app')

@section('content')
    <header id="header">
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <h1><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Profile<small></small></h1>
                </div>
            </div>
        </div>
    </header>
    <section id="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li>
                     <i class="glyphicon glyphicon-home"></i>
                    Dashboard
                </li>
                <li class="active">
                    Profile
                </li>
            </ol>
        </div>
    </section>
    <section id="main">
        <div class="container">
            <div class="row">
            @include('admin.layouts.sidebar')
                <div class="col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-heading main-color-bg">
                            <h3 class="panel-title">Profile</h3>
                        </div>
                        <div class="panel-body">
                        @include('admin.layouts.message')
                        <form class="row" method="post" action="{{ route('profile.update')}}" autocomplete="off" enctype="multipart/form-data">
                             @csrf
                            <div class="col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading main-color-bg">
                                            Avatar
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="file" name="avatar" class="dropify @error('avatar') is-invalid @enderror" data-default-file="{{ asset('storage/user/'.Auth::User()->avatar) }}">
                                                        @error('avatar')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="name">Name *</label>
                                                <input name="name" class="form-control @error('name') is-invalid @enderror" type="text" placeholder="Name" autofocus value="{{ Auth::User()->name }}">
                                                @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror   
                                            </div>
                                        </div>
                                         <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Email *</label>
                                                <input name="email" class="form-control @error('email') is-invalid @enderror" type="text"  placeholder="Email" value="{{  Auth::User()->email  }}">
                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="mobile_no">Mobile No *</label>
                                                <input name="mobile_no" class="form-control @error('mobile_no') is-invalid @enderror" type="text" placeholder="Mobile No" value="{{  Auth::User()->mobile_no  }}">
                                                @error('mobile_no')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="address">Address </label>
                                                <textarea name="address" class="form-control @error('address') is-invalid @enderror" type="text" placeholder="Address" value="{{  Auth::User()->address }}"></textarea>
                                                @error('address')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>   
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="pull-right">  
                                    <button type="submit" class="btn btn-sm btn-round btn-primary">
                                        <i class="glyphicon glyphicon-send"></i>
                                            Update
                                    </button>
                                </div>                                              
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection