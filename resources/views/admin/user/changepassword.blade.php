@extends('admin.layouts.app')

@section('content')
    <header id="header">
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <h1><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Change Password<small></small></h1>
                </div>
            </div>
        </div>
    </header>
    <section id="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li>
                    <i class="glyphicon glyphicon-home"></i>
                    Dashboard
                </li>
                <li class="active">
                    Change Password
                </li>
            </ol>
        </div>
    </section>
    <section id="main">
        <div class="container">
            <div class="row">
            @include('admin.layouts.sidebar')
                <div class="col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-heading main-color-bg">
                            <h3 class="panel-title">Change Password</h3>
                        </div>
                        <div class="panel-body">
                        @include('admin.layouts.message')
                        <form class="row" method="post" action="{{ route('password')}}" autocomplete="off" enctype="multipart/form-data">
                            @csrf
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="current_password">Current Password *</label>
                                        <input id="current_password" type="password" class="form-control{{ $errors->has('current_password') ? ' is-invalid' : '' }}" name="current_password" placeholder="Current Password">
                                        @if ($errors->has('current_password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('current_password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="new_password">New Password *</label>
                                        <input id="new_password" type="password" class="form-control{{ $errors->has('new_password') ? ' is-invalid' : '' }}" name="new_password" placeholder="New Password">
                                        @if ($errors->has('new_password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('new_password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="confirm_password">Confirm Password *</label>
                                        <input id="confirm_password" type="password" class="form-control{{ $errors->has('confirm_password') ? ' is-invalid' : '' }}" name="confirm_password" placeholder="Confirm Password">
                                        @if ($errors->has('confirm_password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('confirm_password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="pull-right">  
                                    <button type="submit" class="btn btn-sm btn-round btn-primary">
                                        <i class="glyphicon glyphicon-send"></i>
                                            Update
                                    </button>
                                </div>                                              
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection