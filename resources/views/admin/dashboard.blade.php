@extends('admin.layouts.app')
 
@section('content')
    <header id="header">
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <h1><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Dashboard</h1>
                </div>
            </div>
        </div>
    </header>
    <section id="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li>
                    <i class="glyphicon glyphicon-home"></i>
                    Dashboard
                </li>
            </ol>
        </div>
    </section>
    <section id="main">
        <div class="container">
            <div class="row">
            @include('admin.layouts.sidebar')
            <div class="col-md-9">
            <div class="panel panel-default">
              <div class="panel-heading main-color-bg">
                <h3 class="panel-title">Website Overview</h3>
              </div>
              <div class="panel-body">
                <div class="col-md-3">
                  <div class="well dash-box">
                    <h2><span class="glyphicon glyphicon-user" aria-hidden="true"></span><br>1 </h2>
                    <h4>User</h4>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="well dash-box">
                    <h2><span class="glyphicon glyphicon-picture" aria-hidden="true"></span><br> {{$albumcount}}</h2>
                    <h4>Alubums</h4>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="well dash-box">
                    <h2><span class="glyphicon glyphicon-picture" aria-hidden="true"><br></span> <br> {{$photocount}}</h2>
                    <h4>Photos</h4>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="well dash-box">
                    <h2><span class="glyphicon glyphicon-list-alt" aria-hidden="true"><br></span><br> 1</h2>
                    <h4>Organization</h4>
                  </div>
                </div>
              </div>
              </div>
          </div>
            </div>
        </div>
    </section>
@endsection
