@extends('admin.layouts.app')

@section('content')
    <header id="header">
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <h1><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Albums</h1>
                </div>
                <div class="col-md-2 text-right">
                    <div class="create">
                        <a href="{{ route('albums.index') }}" class="btn btn-sm  btn-success" type="button">
                            <i class="glyphicon glyphicon-list"></i>
                                Albums
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section id="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li>
                    <i class="glyphicon glyphicon-home"></i>
                    Dashboard
                </li>
                <li class="active">
                    Albums
                </li>
            </ol>
        </div>
    </section>
    <section id="main">
       <div class="container">
            <div class="row">
                @include('admin.layouts.sidebar')
                <div class="col-md-9">
                    <div class="panel panel-default">
                    @include('admin.layouts.message')
                        {{-- <div class="panel-heading main-color-bg">
                            <h3 class="panel-title">Images</h3>
                        </div> --}}
                        <div class="panel-body">
                            <form class="col-sm-4" method="post" action="{{ route('photos.store') }}" autocomplete="off" enctype="multipart/form-data">
                                @csrf
                                <div class="panel panel-default">
                                    <div class="panel-heading main-color-bg">
                                        New Image
                                    </div>
                                    <div class="">
                                        <input type="hidden" name="album_id" value="{{ $album->album_id }}">
                                        <div class="form-group">
                                            <input type="file" id="images" name="images[]" class="dropify @error('title') is-invalid @enderror" multiple>
                                            @error('images')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>  
                                    </div>
                                    <div class="panel-footer">
                                        <div class="float-right"> 
                                            <a href="{{ route('albums.index')}}" class="btn btn-sm btn-danger ">
                                                <i class="glyphicon glyphicon-ban-circle"></i> Discard
                                            </a>
                                            <button type="submit" class="btn btn-sm btn-primary">
                                                <i class="glyphicon glyphicon-send"></i>
                                                Submit
                                            </button>
                                        </div>                                              
                                    </div>
                                </div>
                            </form>
                            <div class="col-sm-8">
                                <div class="panel panel-default">
                                    <div class="panel-heading main-color-bg ">
                                        Images
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            @foreach($images as $image)
                                                <div class="col-sm-4">
                                                    <div class="panel panel-default">
                                                        <img class="panel-img-top" src="{{ asset('storage/photos/'.$image->image) }}" alt="{{ $image->title }}" height="130px" width="140px">
                                                        <div class="panel-footer">
                                                            <button onclick="destroy({{ $image->photo_id }})" type="button" class="btn btn-sm btn-block btn-danger">
                                                                 <i class='glyphicon glyphicon-trash'></i>
                                                                Delete
                                                            </button>
                                                         </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script type="text/javascript">
        function destroy(photo_id) 
        {
            if(confirm('Do you want to Continue...?'))
            {
                $.ajax({
                    method: 'DELETE',
                    url: "{{ route('photos.destroy','photo_id') }}".replace('photo_id',photo_id),
                    data: {
                        _token: '{{ Session::token() }}'
                    },
                    success: function(data)
                    {
                        location.reload();
                    },
                    error: function(data)
                    {
                        console.log(data);
                    }
                });
            }
        }
    </script>
@endsection
