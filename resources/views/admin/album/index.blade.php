@extends('admin.layouts.app')

@section('content')
    <header id="header">
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <h1><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Albums</h1>
                </div>
                <div class="col-md-2 text-right">
                    <div class="create ">
                        <a href="{{ route('albums.create') }}" class="btn btn-sm  btn-success" type="button">
                            <i class="glyphicon glyphicon-plus"></i>
                                New Album
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section id="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li>
                    <i class="glyphicon glyphicon-home"></i>
                    Dashboard
                </li>
                <li class="active">
                    Albums
                </li>
            </ol>
        </div>
    </section>
    <section id="main">
        <div class="container">
            <div class="row">
                @include('admin.layouts.sidebar')
                <div class="col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-heading main-color-bg">
                            <h3 class="panel-title">Albums</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                @foreach($albums as $album)
                                    <div class="col-sm-4 pb-3">
                                        <div class="panel panel-default">
                                            <img class="panel-img-top" src="{{ asset('storage/album/'.$album->poster) }}" height="200px" width="251px" alt="{{ $album->title }}">
                                            <div class="panel-body">
                                                <h5 class="panel-title">{{ $album->title }}</h5>
                                                <p class="panel-text">{!! str_limit($album->description, 80) !!}</p>
                                            </div>
                                            <div class="panel-footer text-muted text-center">
                                                <a href="{{ route('albums.show',$album->album_id) }}" class="btn btn-xs btn-primary">
                                                    <i class='glyphicon glyphicon-plus'></i>
                                                    Images
                                                </a>
                                                <a href="{{ route('albums.edit',$album->album_id) }}" class="btn btn-xs btn-success">
                                                     <i class='glyphicon glyphicon-pencil'></i>
                                                    Edit
                                                </a>
                                                <button onclick="destroy({{ $album->album_id }})" type="button" class="btn btn-xs btn-danger">
                                                     <i class='glyphicon glyphicon-trash'></i>
                                                    Delete
                                                </button>
                                             </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script type="text/javascript">
        function destroy(album_id) 
        {
            if(confirm('Do you want to Continue...?'))
            {
                $.ajax({
                    method: 'DELETE',
                    url: "{{ route('albums.destroy','album_id') }}".replace('album_id',album_id),
                    data: {
                        _token: '{{ Session::token() }}'
                    },
                    success: function(data)
                    {
                        window.location='{{ route("albums.index") }}';
                    },
                    error: function(data)
                    {
                        console.log(data);
                    }
                });
            }
        }
    </script>
@endsection