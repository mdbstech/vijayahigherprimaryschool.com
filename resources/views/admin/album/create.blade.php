@extends('admin.layouts.app')

@section('content')
    <header id="header">
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <h1><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Albums</h1>
                </div>
                <div class="col-md-2 text-right">
                    <div class="create">
                        <a href="{{ route('albums.index') }}" class="btn btn-sm  btn-success" type="button">
                            <i class="glyphicon glyphicon-list"></i>
                                Albums
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section id="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li>
                    <i class="glyphicon glyphicon-home"></i>
                    Dashboard
                </li>
                <li class="active">
                    Albums
                </li>
            </ol>
        </div>
    </section>
    <section id="main">
        <div class="container">
            <div class="row">
            @include('admin.layouts.sidebar')
                <div class="col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-heading main-color-bg">
                            <h3 class="panel-title">New Album</h3>
                        </div>
                        <div class="panel-body">
                        @include('admin.layouts.message')
                            <form class="row" method="post" action="{{ route('albums.store') }}" autocomplete="off" enctype="multipart/form-data">
                            @csrf
                                <div class="col-sm-4">
                                    <div class="panel panel-default">
                                      
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="file" name="poster" class="dropify @error('poster') is-invalid @enderror" data-default-file="{{ asset('storage/album/poster.png') }}">
                                                            @error('poster')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="row">
                                           <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="title">Title *</label>
                                                   <input name="title" id="title" class="form-control @error('title') is-invalid @enderror" type="text" placeholder="Title" autofocus value="{{ old('title') }}" onkeyup="convertToSlug('title','slug')">
                                                    @error('title')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                             <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="slug">Slug *</label>
                                                     <input name="slug" id="slug" class="form-control @error('slug') is-invalid @enderror" type="text" placeholder="Slug" value="{{ old('slug') }}" readonly>
                                                    @error('slug')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                             <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="description">Description </label>
                                                    <textarea name="description" class="form-control @error('description') is-invalid @enderror" type="text" placeholder="Description" value="{{old('description')}}"></textarea>
                                                    @error('description')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="pull-right">  
                                        <a href="{{ route('albums.index')}}" class="btn btn-sm btn-danger mr-2">
                                            <i class="glyphicon glyphicon-ban-circle"></i> Discard
                                        </a>
                                        <button type="submit" class="btn btn-sm btn-primary">
                                            <i class="glyphicon glyphicon-send"></i>
                                                Submit
                                        </button>
                                    </div>                                              
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection