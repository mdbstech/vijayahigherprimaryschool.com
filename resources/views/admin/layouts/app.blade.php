<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<title>Vijaya Higher Primary School</title>
       	<link rel="shortcut icon" href="{{ asset ('img/fav.png') }}" type="image/x-icon">
    	<link rel="icon" href="{{ asset ('img/fav.png') }}" type="image/x-icon">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		
		<link href="{{ asset('admin/css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('admin/css/style.css') }}" rel="stylesheet">
		<link rel="stylesheet" href="{{ asset('vendors/dropify/dist/css/dropify.min.css') }}">
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
		<link rel="stylesheet" type="text/css" href=" https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css ">
		<script src="http://cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>
		 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

        <link href="https://raw.githack.com/ttskch/select2-bootstrap4-theme/master/dist/select2-bootstrap4.css" rel="stylesheet">
	</head>
    <body>
		<nav class="navbar navbar-default">
		    <div class="container">
				<div class="navbar-header">
				    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
				  	</button>
				  	<a class="navbar-brand" href="#">Vijaya Higher Primary School</a>
				</div>
				<div id="navbar" class="collapse navbar-collapse">
				    <ul class="nav navbar-nav">
						<li class=""><a href="{{ url('/dashboard') }}">Dashboard</a></li>
						<li><a href="{{ route('organizations.index')}}">Organization</a></li>
			            <li><a href="{{ route('albums.index') }}">Albums</a></li>
				  	</ul>
				    <ul class="nav navbar-nav navbar-right">
					  	@guest
			                <li><a href="{{ route('login') }}">Login</a></li>
			            @else
			            <div class="dropdown create">
			                <a class="btn btn-default dropdown-toggle" type="button"  data-toggle="dropdown"  aria-expanded="true">
			                 	<span>{{ Auth::user()->name }}</span> 
			                 	 <img src="{{ asset('/storage/user/'.Auth::User()->avatar) }}" width="25px" height="20px;" class="img-circle">
			                	<span class="caret"></span>
			            	</a>
			              <ul class="dropdown-menu" >
			               <li>
			                	<a type="button" href="{{ route('profile') }}" >
			                	    <i class="glyphicon glyphicon-user">.</i> 
	                                Profile
	                            </a>
	                        </li>
	                        <li>
	                        	<a type="button" href="{{ route('changepassword') }}" > 
	                        		<i class="glyphicon glyphicon-lock"></i>
	                                Change Password
	                            </a>
	                        </li>
	                         <li>
								<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
										<i class="glyphicon glyphicon-log-out"></i>
											Logout
									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					                    @csrf
					                </form>
				            	</a>
				            </li>
				        </ul>
			            </div>
				        @endif
				  	</ul>
				</div>
		  	</div>
		</nav>
		@yield('content')
		<footer id="footer">
            <div class="container ">
                <div class="center-text">
                    <center class="text-white">
                        <p>© Designed &amp; Developed By <a style="color: #000" href="http://mdbstech.com" target="_blank"> MDBS Tech Pvt. Ltd.</a>
                        </p>
                    </center>
                </div>
            </div>
        </footer>
		<script>
			 CKEDITOR.replace( 'description' );
		</script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="{{ asset('admin/js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('vendors/dropify/dist/js/dropify.min.js') }}"></script>
		<script>
           $(' .dropify').dropify();
            
    </script>
     <script>
            function convertToSlug(from,to)
            {
                 var from_text = $('#' + from).val(); 
                 var to_text = from_text
                    .toLowerCase()
                    .replace('&','and')
                    .replace(/ /g,'-')
                    .replace(/[^\w-]+/g,'');
                    $('#'+to).val(to_text);
            }
        </script>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <script type="text/javascript">
            $(function (){
                $('.select2').select2({
                    theme: 'bootstrap4',
                });
            });

        </script>
   
		 @yield('js')
    </body>
</html>
