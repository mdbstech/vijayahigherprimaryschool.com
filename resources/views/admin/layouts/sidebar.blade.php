<div class="col-md-3">
  <div class="list-group">
      <a href="index.html" class="list-group-item active main-color-bg">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Dashboard
      </a>
      <a href="{{ route('organizations.index')}}" class="list-group-item">
        <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Organizations
      </a>
      <a href="{{ route('profile')}}" class="list-group-item">
        <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Profile
      </a>
      <a href="{{ route('changepassword')}}" class="list-group-item">
        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Chang Password
      </a>
      <a href="{{route('albums.index')}}" class="list-group-item">
        <span class="glyphicon glyphicon-picture" aria-hidden="true"></span> Albums
      </a>
  </div>
</div>