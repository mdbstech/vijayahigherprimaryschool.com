@extends('admin.layouts.app')

@section('content')
    <header id="header">
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <h1><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Organization</h1>
                </div>
            </div>
        </div>
    </header>
    <section id="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    Dashboard
                </li>
                <li class="active">
                    Organization
                </li>
            </ol>
        </div>
    </section>
    <section id="main">
        <div class="container">
            <div class="row">
            @include('admin.layouts.sidebar')
                <div class="col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-heading main-color-bg">
                            <h3 class="panel-title">Organization</h3>
                        </div>
                        <div class="panel-body">
                        @include('admin.layouts.message')
                        <form class="row" method="post" action="{{ route('organizations.update',$org->org_id) }}" autocomplete="off" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                           {{--  <div class="row"> --}}
                                <div class="col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading main-color-bg">
                                            Logo
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="file" name="logo" class="dropify @error('logo') is-invalid @enderror" data-default-file="{{ asset('storage/organization/'.$org->logo) }}">
                                                        @error('logo')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="org_code">Organization Code *</label>
                                                <input name="org_code" class="form-control @error('org_code') is-invalid @enderror" type="text" placeholder="Organization Code" autofocus value="{{ $org->org_code }}">
                                                @error('org_code')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror   
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="org_name">Organization Name *</label>
                                                <input name="org_name" class="form-control @error('org_name') is-invalid @enderror" type="text" placeholder="Organization Name" value="{{ $org->org_name }}">
                                                @error('org_name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input name="email" class="form-control @error('email') is-invalid @enderror" type="text"  placeholder="Email" value="{{ $org->email }}">
                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="mobile_no">Mobile No</label>
                                                <input name="mobile_no" class="form-control @error('mobile_no') is-invalid @enderror" type="text" placeholder="Mobile No" value="{{ $org->mobile_no }}">
                                                @error('mobile_no')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="phone_no">Phone No</label>
                                                <input name="phone_no" class="form-control @error('phone_no') is-invalid @enderror" type="text" placeholder="Phone No" value="{{ $org->phone_no }}">
                                                @error('phone_no')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="website">Website</label>
                                                <input name="website" class="form-control @error('website') is-invalid @enderror" type="text" placeholder="Website" value="{{ $org->website }}">
                                                @error('website')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="address">Address</label>
                                                <textarea name="address" class="form-control @error('address') is-invalid @enderror" type="text" placeholder="Address">{{ $org->address }}</textarea>
                                                @error('address')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>   
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="pull-right">  
                                    <button type="submit" class="btn btn-sm btn-round btn-primary">
                                        <i class="glyphicon glyphicon-send"></i>
                                            Update
                                    </button>
                                </div>                                              
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection