@extends('layouts.appfront')

@section('content')

 <!--Page Title-->
    <section class="page-title" style="background-image:url(img/banner.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
              
                <h1>Women's Day Celebration</h1>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Gallery Section -->
    <section class="gallery-section style-two">   
        <div class="auto-container">
            <div class="row">

      
    

                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="400ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/womensday/w2.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/womensday/w2.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>
                </div>

     
                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/womensday/w4.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/womensday/w4.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>
                </div>

                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="400ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/womensday/w5.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/womensday/w5.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>
                </div>

                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/womensday/w6.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/womensday/w6.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>
                </div>

                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/womensday/w7.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/womensday/w7.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>
                </div>

                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="400ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/womensday/w8.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/womensday/w8.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>
                </div>

                <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/womensday/w9.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/womensday/w9.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>


                </div>

                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/womensday/w10.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/womensday/w10.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>


                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/womensday/w11.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/womensday/w11.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>


                    <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/womensday/w12.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/womensday/w12.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>

                               <!-- Gallery Item -->
                <div class="gallery-item col-lg-3 col-md-6 col-sm-12 wow fadeIn" data-wow-delay="800ms">
                    <div class="image-box">
                        <figure class="image"><img src="{{ asset ('img/gallery/womensday/w13.jpg') }}" alt=""></figure>
                        <div class="overlay-box"><a href="{{ asset ('img/gallery/womensday/w13.jpg') }}" class="lightbox-image" data-fancybox='gallery'><span class="icon flaticon-add"></span></a></div>
                    </div>

                    
                </div>

            </div>
        </div>
    </section>
    <!--End Gallery Section -->

@endsection