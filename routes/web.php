<?php




Route::get('/', 'HomeController@home');



Route::get('/about-us', 'HomeController@about');

Route::get('/contact-us', 'HomeController@contact');

Route::get('/gallery', 'HomeController@gallery');

Route::get('/gallery1', 'HomeController@gallery1');

Route::get('/academics', 'HomeController@academics');

Route::get('/facilities', 'HomeController@facilities');

Route::get('/activities', 'HomeController@activities');

Route::get('/staffs', 'HomeController@staffs');

Route::get('/management', 'HomeController@management');

Route::get('/students', 'HomeController@students');

Route::get('/norms', 'HomeController@norms');

Route::get('/circular', 'HomeController@circular');

Route::get('/annual-report', 'HomeController@annualreport');

Route::get('/beyond-academics', 'HomeController@beyondacademics');

Route::get('/gallery2', 'HomeController@gallery2');


Auth::routes();

Route::get('/home', 'HomeController@home')->name('home');
Route::get('/dashboard', 'HomeController@dashboard');
Route::resource('/organizations', 'OrganizationController');
Route::get('/profile','OrganizationController@profile')->name('profile');
Route::post('/profile','OrganizationController@update_profile')->name('profile.update');
Route::get('/changepassword','OrganizationController@changepassword')->name('changepassword');
Route::post('/password', 'OrganizationController@password')->name('password');
Route::resource('/albums','AlbumController');
Route::resource('/photos','PhotoController');